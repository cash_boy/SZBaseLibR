package com.cn.shuangzi.common.umeng;

/**
 * Created by mu on 2018/5/30.
 */

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;

import com.cn.shuangzi.R;
import com.cn.shuangzi.util.SZUtil;


/**
 * Helper class to manage notification channels, and create notifications.
 */
@TargetApi(Build.VERSION_CODES.O)
public class NotificationHelper extends ContextWrapper {
    private NotificationManager manager;
    public String CHANNEL_ID = "message";
    public String CHANNEL_NAME = "消息";

    /**
     * Registers notification channels, which can be used later by individual notifications.
     *
     * @param ctx The application context
     */

    public NotificationHelper(Context ctx) {
        this(ctx,null,null);
    }

    public NotificationHelper(Context ctx, String channelId, String channelName) {
        super(ctx);
        SZUtil.logError("channelId："+channelId+"|channelName："+channelName);
        if(channelId!=null&&channelName!=null) {
            CHANNEL_ID = channelId;
            CHANNEL_NAME = channelName;
        }
        if(getManager().getNotificationChannel(channelId) == null) {
            NotificationChannel chan = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            chan.setLightColor(Color.BLUE);
            chan.enableLights(true);
            chan.enableVibration(true);
            chan.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
            getManager().createNotificationChannel(chan);
        }
    }

    /**
     * Get a notification of type 1
     *
     * Provide the builder rather than the notification it's self as useful for making notification
     * changes.
     *
     * @param title the title of the notification
     * @param body the body text for the notification
     * @return the builder as it keeps a reference to the notification (since API 24)
     */
//    public Notification.Builder getNotification(String title, String body, PendingIntent intentContent, PendingIntent intentDelete) {
//        return new Notification.Builder(getApplicationContext(), PRIMARY_CHANNEL)
//                .setContentTitle(title)
//                .setContentText(body)
//                .setSmallIcon(getSmallIcon())
//                .setAutoCancel(true)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                .setContentIntent(intentContent)
//                .setDeleteIntent(intentDelete);
//    }

    /**
     * Build notification for secondary channel.
     *
     * @param title Title for notification.
     * @param body  Message for notification.
     * @return A Notification.Builder configured with the selected channel and details
     */
    public Notification.Builder getNotificationNoDeleteIntent(String title, String body, PendingIntent intent,int smallIconResId) {

        return new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(smallIconResId)
                .setContentIntent(intent)
                .setAutoCancel(true);
    }

    /**
     * Send a notification.
     *
     * @param id           The ID of the notification
     * @param notification The notification object
     */
    public void notify(int id, Notification.Builder notification) {
        getManager().notify(id, notification.build());
    }

    /**
     * Get the notification manager.
     * <p>
     * Utility method as this helper works with it a lot.
     *
     * @return The system service NotificationManager
     */
    private NotificationManager getManager() {
        if (manager == null) {
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }
}
