package com.cn.shuangzi.common.umeng;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class PushInfo implements Serializable {
    public static final String ACTION_PAGE = "PAGE";
    public static final String ACTION_WEB = "WEB_VIEW";
    private String channel;
    private String action;//PAGE,WEB_VIEW
    private String extended;
    private String target;//recordDetails

    public String getAction() {
        return action;
    }

    public <T> T getExtended(Class<T> classOfT) {
        return new Gson().fromJson(extended,classOfT);
    }

    public <T> T getChannelT(Class<T> classOfT) {
        return new Gson().fromJson(channel,classOfT);
    }

    public ChannelInfo getChannel() {
        return getChannelT(ChannelInfo.class);
    }

    public String getChannelName(){
        try {
            return getChannel().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getChannelId(){
        try {
            return getChannel().getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return "PushInfo{" +
                "channel='" + channel + '\'' +
                ", action='" + action + '\'' +
                ", extended='" + extended + '\'' +
                ", target='" + target + '\'' +
                '}';
    }
}
