package com.cn.shuangzi.common.umeng;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;

import com.cn.shuangzi.SZBaseWebActivity;
import com.cn.shuangzi.util.SZUtil;
import com.google.gson.Gson;
import com.umeng.message.UmengMessageService;
import com.umeng.message.entity.UMessage;

import org.android.agoo.common.AgooConstants;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class SZUmengNotificationService extends UmengMessageService {
    @Override
    public void onMessage(Context context, Intent intent) {
        String message = intent.getStringExtra(AgooConstants.MESSAGE_BODY);
        SZUtil.log("收到新消息:"+message);

        try {
            UMessage umsg = new UMessage(new JSONObject(message));
            PushInfo pushInfo = new Gson().fromJson(new JSONObject(umsg.extra).toString(), PushInfo.class);
            showNotification(pushInfo,umsg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showNotification(PushInfo pushInfo, UMessage umsg) {
        try {
            Intent resultIntent = new Intent(this,getUMessageActivity());
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);// 关键的一步，设置启动模式
            resultIntent.putExtra("UMessage",pushInfo);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(
                    this,(int) System.currentTimeMillis(), resultIntent,
//                    PendingIntent.FLAG_IMMUTABLE);//PendingIntent.FLAG_IMMUTABLE表示可以不可以在运行时被修改，可以被共享给其他应用使用
                    PendingIntent.FLAG_MUTABLE| PendingIntent.FLAG_UPDATE_CURRENT);//PendingIntent.FLAG_MUTABLE表示可以在运行时被修改，并且不需要被共享给其他应用使用

            if (Build.VERSION.SDK_INT >= 26) {
                NotificationHelper notificationHelper = new NotificationHelper(this,pushInfo.getChannelId(),pushInfo.getChannelName());
                notificationHelper.notify((int) System.currentTimeMillis(), notificationHelper.getNotificationNoDeleteIntent(umsg.title,umsg.text,resultPendingIntent,getSmallIcon()));
                lightScreenOnce();
            } else {
                Notification.Builder mBuilder = new Notification.Builder(this);
                mBuilder.setContentTitle(umsg.title)
                        .setContentText(umsg.text)
                        .setWhen(System.currentTimeMillis())
                        .setSmallIcon(getSmallIcon())
                        .setAutoCancel(true);
                Notification notification = mBuilder.getNotification();
                notification.contentIntent = resultPendingIntent;
                notification.defaults = Notification.DEFAULT_ALL;
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify((int) System.currentTimeMillis(), notification);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    private void lightScreenOnce(){
        try {
            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock((PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.SCREEN_BRIGHT_WAKE_LOCK), "Notification");
            wl.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public abstract int getSmallIcon();
    public abstract Class<? extends SZUMessageActivity> getUMessageActivity();
}
