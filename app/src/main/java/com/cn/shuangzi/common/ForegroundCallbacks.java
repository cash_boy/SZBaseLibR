package com.cn.shuangzi.common;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;

import com.cn.shuangzi.util.SZUtil;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ForegroundCallbacks implements Application.ActivityLifecycleCallbacks {

    private static long CHECK_DELAY = 1000 * 60;
    private static ForegroundCallbacks instance;
    private boolean foreground = false, paused = true;
    private Handler handler = new Handler();
    private List<Listener> listeners = new CopyOnWriteArrayList<>();
    private Runnable check;
    private boolean isFirstInit = true;
    private boolean isUseAppInitMode = true;

    public static ForegroundCallbacks init(Application application) {
        if (instance == null) {
            instance = new ForegroundCallbacks();
            application.registerActivityLifecycleCallbacks(instance);
        }
        return instance;
    }

    public static ForegroundCallbacks init(Application application, long timeInterval) {
        CHECK_DELAY = timeInterval;
        if (instance == null) {
            instance = new ForegroundCallbacks();
            application.registerActivityLifecycleCallbacks(instance);
        }
        return instance;
    }

    public ForegroundCallbacks setAppInitMode(boolean isUseAppInitMode) {
        this.isUseAppInitMode = isUseAppInitMode;
        return instance;
    }

    public static ForegroundCallbacks get() {
        return instance;
    }

    public boolean isForeground() {
        return foreground;
    }

    public boolean isBackground() {
        return !foreground;
    }

    public void addListenerOnly(Listener listener) {
        listeners.clear();
        addListener(listener);
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;
        if (check != null) {
            handler.removeCallbacks(check);
        }
        if (wasBackground) {
            if (isUseAppInitMode) {
                if (isFirstInit) {
                    isFirstInit = false;
                    return;
                }
            }
            for (Listener l : listeners) {
                try {
                    l.onBecameForeground();
                } catch (Exception exc) {
                    SZUtil.log("切换前台异常：" + exc.getMessage());
                }
            }
        }
    }

    @Override
    public void onActivityPaused(final Activity activity) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!SZUtil.isAppForeground(activity)) {
                    paused = true;
                    if (check != null) {
                        handler.removeCallbacks(check);
                    }
                    handler.postDelayed(check = new Runnable() {
                        @Override
                        public void run() {
                            if (foreground && paused) {
                                foreground = false;
                            }
                        }
                    }, CHECK_DELAY);
                    for (Listener l : listeners) {
                        try {
                            l.onBecameBackground();
                        } catch (Exception exc) {
                            SZUtil.log("切换到后台异常：" + exc.getMessage());
                        }
                    }
                }
            }
        }, 100);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public interface Listener {
        void onBecameForeground();

        void onBecameBackground();
    }

}
