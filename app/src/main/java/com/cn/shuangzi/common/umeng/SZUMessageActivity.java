package com.cn.shuangzi.common.umeng;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.cn.shuangzi.util.SZUtil;
import com.google.gson.Gson;
import com.umeng.message.UmengNotifyClickActivity;
import com.umeng.message.entity.UMessage;

import org.android.agoo.common.AgooConstants;
import org.json.JSONObject;

import androidx.annotation.Nullable;

/**
 * Created by CN.
 */

public abstract class SZUMessageActivity extends UmengNotifyClickActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//21表示5.0
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//19表示4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        try {
            PushInfo pushInfo = (PushInfo) getIntent().getSerializableExtra("UMessage");
            SZUtil.logError("打开消息:" + pushInfo);
            if(pushInfo!=null){
                dealMessage(pushInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    @Override
    public void onMessage(Intent intent) {
        super.onMessage(intent);
        if (intent != null) {
            final String body = intent.getStringExtra(AgooConstants.MESSAGE_BODY);
            SZUtil.logError("厂商消息body:" + body);
            try {
                String message = new JSONObject(new UMessage(new JSONObject(body)).extra).toString();
                PushInfo pushInfo = new Gson().fromJson(message, PushInfo.class);
                SZUtil.logError("厂商消息pushInfo:" + pushInfo);
                dealMessage(pushInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        finish();
    }

    public abstract void dealMessage(PushInfo pushInfo);
}
