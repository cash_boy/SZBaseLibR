package com.cn.shuangzi.common;

import com.cn.shuangzi.util.SZDateUtil;

/**
 * Created by CN.
 */

public class SZConst {
    public static final int TIME_OUT = 12000;
    public static final String API_SERVICE_ERROR_EVENT_ID = "api_service_error";
    public static final String PLATFORM_LOCAL = "local";
    public static final String ERROR_ID = "errorId";
    public static final String ERROR_MSG = "errorMsg";
    public static final String ERROR_CODE = "errorCode";
    public static final String SZ_APPID = "SZ_APPID";
    public static final String ENDPOINT = "ENDPOINT";
    public static final String BUCKET = "BUCKET";

    public static final String ALIPAY = "ALIPAY";
    public static final String WXPAY = "WXPAY_APP";
    public static final String GOLD_MEMBER = "GOLD_MEMBER";

    public static final int LIMIT = 30;
    public static final int LIMIT_MAX = 300;
    public static final String USER_INFO_SZ = "sz_user_info";
    public static final String SETTING = "setting";
    public static final String FIRST_LOAD_1 = "first_load_1";

    public static final String YES = "1";
    public static final String NO = "0";
    public static final String VIP_INFO = "vip_info";
    public static final String VIP_BOUGHT = "vip_bought";

    public static final String ALIAS_TYPE = "CUSTOMER_ID";


    public static final long REQUEST_PERMISSION_IN_SPLASH_INTERVAL = 2 * SZDateUtil.ONE_DAY;

    public static final long FIRST_TIME_REQUEST_PERMISSION_INTERVAL = 1 * SZDateUtil.ONE_DAY;

    //点击banner位置会员购买入口
    public static final String EVENT_VIP_CLICK_BANNER = "vip_click_banner";
    //banner位置显示购买会员入口
    public static final String EVENT_VIP_SHOW_BANNER = "vip_show_banner";
    //首页动图位置显示购买会员入口
    public static final String EVENT_VIP_SHOW_HOME_GIF = "vip_show_home_gif";
    //点击首页动图位置购买会员入口
    public static final String EVENT_VIP_CLICK_HOME_GIF = "vip_click_home_gif";
    //关闭插屏后显示购买会员入口
    public static final String EVENT_VIP_SHOW_CLOSE_INTERACTION_AD = "vip_show_close_interaction_ad";
    //点击关闭插屏后显示的购买会员入口
    public static final String EVENT_VIP_CLICK_CLOSE_INTERACTION_AD = "vip_click_close_interaction_ad";
    //点击关闭插屏后显示的看视频免广告入口
    public static final String EVENT_VIDEO_TASK_CLICK_CLOSE_INTERACTION_AD = "video_click_close_interaction_ad";
    //购买会员页显示
    public static final String EVENT_BUY_VIP_PAGE_SHOW = "buy_vip_page_show";
    //挽留会员弹窗显示
    public static final String EVENT_RETENTION_VIP_SHOW = "retention_vip_show";
    //点击购买挽留会员
    public static final String EVENT_CLICK_RETENTION_VIP = "click_retention_vip";
    //点击会员sku
    public static final String EVENT_CLICK_VIP_SKU = "click_vip_sku";
    //点击上传图片功能的购买会员
    public static final String EVENT_VIP_CLICK_IMG_ADD = "vip_click_img_add";


    //上传图片功能展示购买会员
    public static final String EVENT_VIP_SHOW_IMG_ADD = "vip_show_img_add";


    //展示视频任务弹窗
    public static final String EVENT_SHOW_VIDEO_TASK = "show_video_task";
    //点击banner的视频任务
    public static final String EVENT_CLICK_BANNER_VIDEO_TASK = "click_banner_video_task";
    //执行视频任务
    public static final String EVENT_EXECUTE_VIDEO_TASK = "event_execute_video_task";

    //点击其他会员功能购买会员
    public static final String EVENT_VIP_CLICK_OTHER = "vip_click_other";
    //其他会员功能展示购买会员
    public static final String EVENT_VIP_SHOW_OTHER = "vip_show_other";
}
