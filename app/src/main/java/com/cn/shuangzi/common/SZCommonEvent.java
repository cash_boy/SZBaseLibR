package com.cn.shuangzi.common;

/**
 * Created by CN.
 */

public class SZCommonEvent<T extends Object> {
    private int event;
    private T avObject;
    private T avObject2;

    public SZCommonEvent(int event) {
        this.event = event;
    }

    public SZCommonEvent(int event, T avObject) {
        this.event = event;
        this.avObject = avObject;
    }

    public SZCommonEvent(int event, T avObject, T avObject2) {
        this.event = event;
        this.avObject = avObject;
        this.avObject2 = avObject2;
    }

    public T getObject2() {
        return avObject2;
    }

    public void setObject2(T avObject2) {
        this.avObject2 = avObject2;
    }

    public void setObject(T object) {
        this.avObject = object;
    }

    public <T extends Object> T getObject() {
        return (T) avObject;
    }

    public int getEvent() {
        return event;
    }

    public static final int LOGIN_EVENT = 0x9001;
    public static final int LOGOUT_EVENT = 0x9002;
    public static final int MODIFY_NICKNAME_EVENT = 0x9003;
    public static final int MODIFY_AVATAR_EVENT = 0x9004;
    public static final int MODIFY_USER_INFO_EVENT = 0x9005;
    public static final int BIND_PHONE_EVENT = 0x9006;
    public static final int BIND_WECHAT_EVENT = 0x9007;

    @Override
    public String toString() {
        return "SZCommonEvent{" +
                "event=" + event +
                ", avObject=" + avObject +
                ", avObject2=" + avObject2 +
                '}';
    }
}
