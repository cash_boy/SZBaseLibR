package com.cn.shuangzi.common;

/**
 * Created by CN.
 */

public class AppIdEmptyException extends IllegalArgumentException {
    public AppIdEmptyException() {
        super("SZ_APP_ID not set in AndroidManifest...");
    }
}
