package com.cn.shuangzi.common.umeng;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class ChannelInfo implements Serializable {
    private String data;
    private String name;
    public String getId(){
        if(data!=null){
            try {
                ChannelExtraInfo channelExtraInfo = new Gson().fromJson(data,ChannelExtraInfo.class);
                return channelExtraInfo.getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getName(){
        return name;
    }
    public class ChannelExtraInfo implements Serializable {
        private String id;

        public String getId() {
            return id;
        }

        @Override
        public String toString() {
            return "ChannelExtraInfo{" +
                    "id='" + id + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ChannelInfo{" +
                "data='" + data + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
