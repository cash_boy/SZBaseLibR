package com.cn.shuangzi.common;

import java.io.Serializable;
import java.util.List;

/**
 * Created by CN.
 */

public class PageInfo<T extends Object> implements Serializable {
    private int recordsTotalPage;
    private List<T> data;

    public int getRecordsTotalPage() {
        return recordsTotalPage;
    }

    public void setRecordsTotalPage(int recordsTotalPage) {
        this.recordsTotalPage = recordsTotalPage;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
