package com.cn.shuangzi.permission;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.permission.callback.OnExternalStorageManagerPermissionCallback;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;


/**
 * Created by CN on 2017-10-11.
 */

public class RequestExternalStorageManagePermissionHelper {
    private SZBaseActivity activity;
    private String title = "开启 “访问所有文件” 权限";
    private String content = "需要使用 “访问所有文件” 权限，以进行文件保存！";
    private int requestCode = CODE_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;
    private OnExternalStorageManagerPermissionCallback onExternalStorageManagerPermissionCallback;
    public static final int CODE_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION = 190000;

    public RequestExternalStorageManagePermissionHelper(SZBaseActivity activity, String title, String content, int requestCode, OnExternalStorageManagerPermissionCallback onExternalStorageManagerPermissionCallback) {
        this.activity = activity;
        this.title = title;
        this.content = content;
        this.requestCode = requestCode;
        this.onExternalStorageManagerPermissionCallback = onExternalStorageManagerPermissionCallback;
    }

    public RequestExternalStorageManagePermissionHelper(SZBaseActivity activity, String title, String content, OnExternalStorageManagerPermissionCallback onExternalStorageManagerPermissionCallback) {
        this.activity = activity;
        this.title = title;
        this.content = content;
        this.onExternalStorageManagerPermissionCallback = onExternalStorageManagerPermissionCallback;
    }

    public RequestExternalStorageManagePermissionHelper(SZBaseActivity activity, String content, OnExternalStorageManagerPermissionCallback onExternalStorageManagerPermissionCallback) {
        this.activity = activity;
        this.content = content;
        this.onExternalStorageManagerPermissionCallback = onExternalStorageManagerPermissionCallback;
    }

    public RequestExternalStorageManagePermissionHelper(SZBaseActivity activity, OnExternalStorageManagerPermissionCallback onExternalStorageManagerPermissionCallback) {
        this.activity = activity;
        this.onExternalStorageManagerPermissionCallback = onExternalStorageManagerPermissionCallback;
    }

    public void onActivityResult(int requestCode) {
        if (requestCode == this.requestCode) {
            if (SZUtil.isAndroidR()) {
                if (Environment.isExternalStorageManager()) {
                    onExternalStorageManagerPermissionCallback.onPermissionRequestSuccess(requestCode);
                } else {
                    onExternalStorageManagerPermissionCallback.onPermissionRequestRefuse(requestCode);
                }
            }
        }
    }

    public void checkPermission() {

        if (SZUtil.isAndroidR()) {
            if (Environment.isExternalStorageManager()) {
                if (onExternalStorageManagerPermissionCallback != null) {
                    onExternalStorageManagerPermissionCallback.onPermissionRequestSuccess(requestCode);
                }
            }else {
                activity.showAlert(SweetAlertDialog.WARNING_TYPE, title, content, "去开启", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        openExternalStorageManagerPage();
                    }
                }, "拒绝", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        if (onExternalStorageManagerPermissionCallback != null) {
                            onExternalStorageManagerPermissionCallback.onPermissionRequestRefuse(requestCode);
                        }
                    }
                }, true);
            }
        }
    }

    private void openExternalStorageManagerPage() {
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
        intent.setData(Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, requestCode);
    }
}
