package com.cn.shuangzi.permission.callback;

public interface OnExternalStorageManagerPermissionCallback {
    void onPermissionRequestSuccess(int requestCode);
    void onPermissionRequestRefuse(int requestCode);

}
