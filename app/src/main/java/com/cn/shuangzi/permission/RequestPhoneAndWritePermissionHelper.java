package com.cn.shuangzi.permission;

import android.Manifest;
import android.app.Activity;

import com.cn.shuangzi.R;


/**
 * 动态申请必须的基本权限
 * Created by CN on 2017-10-20.
 */

public class RequestPhoneAndWritePermissionHelper extends RequestPermissionHelper {
    private static final String[] permissionArrays = {Manifest.permission.READ_PHONE_STATE, Manifest.permission
            .WRITE_EXTERNAL_STORAGE};
    private static final int[] permissionInfo = {R.string.open_read_phone_state_permit, R.string.open_storage_permit};
    public RequestPhoneAndWritePermissionHelper(Activity context, boolean isAlertOnPermissionReallyDeclined, RequestPermissionViewP requestPermissionViewP) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined);
    }
    public RequestPhoneAndWritePermissionHelper(Activity context, int[] permissionInfo, boolean isAlertOnPermissionReallyDeclined, RequestPermissionViewP requestPermissionViewP) {
        super(context, requestPermissionViewP, permissionArrays, permissionInfo,isAlertOnPermissionReallyDeclined);
    }

}
