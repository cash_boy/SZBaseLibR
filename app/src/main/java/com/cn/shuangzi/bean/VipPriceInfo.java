package com.cn.shuangzi.bean;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by CN.
 */

public class VipPriceInfo implements Serializable {
    private String fpApplicationId;
    private String fpId;
    private String fpPrice;
    private String fpName;
    private String fpSubject;
    private String fpTag;
    private String fpOriginalPrice;
    private String fpRealOriginalPrice;
    private String[] fpPayType;
    private String fpMode;
    private String fpExtendedData;
    private String fpBuyLimit;
    private ExtendVipInfo extendVipInfo;
    private boolean isCheck;
    private boolean fpIsSubscription;

    public boolean hasWechatPay() {
        if (fpPayType != null) {
            for (String type : fpPayType) {
                if ("WXPAY_APP".equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasAliPay() {
        if (fpPayType != null) {
            for (String type : fpPayType) {
                if ("ALIPAY".equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isBuyLimit() {
        return "ONLY_ONE".equalsIgnoreCase(fpBuyLimit);
    }

    public boolean isSubscription() {
        return fpIsSubscription;
    }

    public String getFpMode() {
        return fpMode;
    }

    public boolean isSpecialPrice() {
        return "discount".equalsIgnoreCase(fpMode);
    }

    public boolean isRetention() {
        return "RETENTION_SALE".equalsIgnoreCase(fpMode);
    }

    public class ExtendVipInfo implements Serializable {
        private String discountDesc;
        private String notes;
        private boolean hasDeleteLine;
        private boolean isForever;

        public String getNotes() {
            return notes;
        }

        public String getDiscountDesc() {
            return discountDesc;
        }

        public boolean hasDeleteLine() {
            return hasDeleteLine;
        }

        public boolean isForever() {
            return isForever;
        }
    }

    public String getFpRealOriginalPrice() {
        return fpRealOriginalPrice;
    }

    public String[] getFpPayType() {
        return fpPayType;
    }

    public String getFpExtendedData() {
        return fpExtendedData;
    }

    public ExtendVipInfo getExtendedData() {
        if (extendVipInfo == null) {
            extendVipInfo = new Gson().fromJson(fpExtendedData, ExtendVipInfo.class);
        }
        return extendVipInfo;
    }

    public String getDiscountDesc() {
        if (getExtendedData() != null) {
            return getExtendedData().getDiscountDesc();
        }
        return null;
    }

    public String getNotes() {
        if (getExtendedData() != null) {
            return getExtendedData().getNotes();
        }
        return null;
    }

    public boolean hasDeleteLine() {
        if (getExtendedData() != null) {
            return getExtendedData().hasDeleteLine();
        }
        return false;
    }

    public boolean isForever() {
        if (getExtendedData() != null) {
            return getExtendedData().isForever();
        }
        return false;
    }

    public void setFpApplicationId(String fpApplicationId) {
        this.fpApplicationId = fpApplicationId;
    }

    public void setFpId(String fpId) {
        this.fpId = fpId;
    }

    public void setFpPrice(String fpPrice) {
        this.fpPrice = fpPrice;
    }

    public void setFpName(String fpName) {
        this.fpName = fpName;
    }

    public void setFpSubject(String fpSubject) {
        this.fpSubject = fpSubject;
    }

    public void setFpTag(String fpTag) {
        this.fpTag = fpTag;
    }

    public void setFpOriginalPrice(String fpOriginalPrice) {
        this.fpOriginalPrice = fpOriginalPrice;
    }

    public String getFpApplicationId() {
        return fpApplicationId;
    }

    public String getFpId() {
        return fpId;
    }

    public String getFpPrice() {
        return fpPrice;
    }

    public String getFpName() {
        return fpName;
    }

    public String getFpSubject() {
        return fpSubject;
    }

    public String getFpTag() {
        return fpTag;
    }

    public String getFpOriginalPrice() {
        return fpOriginalPrice;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "VipPriceInfo{" +
                "fpApplicationId='" + fpApplicationId + '\'' +
                ", fpId='" + fpId + '\'' +
                ", fpPrice='" + fpPrice + '\'' +
                ", fpName='" + fpName + '\'' +
                ", fpSubject='" + fpSubject + '\'' +
                ", fpTag='" + fpTag + '\'' +
                ", fpOriginalPrice='" + fpOriginalPrice + '\'' +
                ", fpRealOriginalPrice='" + fpRealOriginalPrice + '\'' +
                ", fpPayType=" + Arrays.toString(fpPayType) +
                ", fpExtendedData='" + fpExtendedData + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
