package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class VipOrderInfo implements Serializable {
    private String poApplicationId;
    private String poConsumerId;
    private String poDeviceId;
    private String poName;
    private String poSubject;
    private double poPrice;
    private String poType;
    private String poId;
    private boolean poIsPay;
    private boolean poIsRefund;

    public String getPoSubject() {
        return poSubject;
    }

    public double getPoPrice() {
        return poPrice;
    }

    public String getPoId() {
        return poId;
    }

    public String getPoApplicationId() {
        return poApplicationId;
    }

    public String getPoConsumerId() {
        return poConsumerId;
    }

    public String getPoDeviceId() {
        return poDeviceId;
    }

    public String getPoName() {
        return poName;
    }

    public String getPoType() {
        return poType;
    }

    public boolean isPoIsRefund() {
        return poIsRefund;
    }

    public boolean isPoIsPay() {
        return poIsPay;
    }

    @Override
    public String toString() {
        return "VipOrderInfo{" +
                "poApplicationId='" + poApplicationId + '\'' +
                ", poConsumerId='" + poConsumerId + '\'' +
                ", poDeviceId='" + poDeviceId + '\'' +
                ", poName='" + poName + '\'' +
                ", poSubject='" + poSubject + '\'' +
                ", poPrice=" + poPrice +
                ", poType='" + poType + '\'' +
                ", poId='" + poId + '\'' +
                ", poIsPay=" + poIsPay +
                ", poIsRefund=" + poIsRefund +
                '}';
    }
//    "poApplicationId":"88b08235d72f11e88e7600163e061e07",
//            "poConsumerId":"33c4648ba2708f9bef2522cda6dc9430",
//            "poCreateTime":1629025437000,
//            "poDeviceId":"ae92eccef9794aa38ab2bbcb0695c524",
//            "poFpId":"000186792d9811e9889c00163e061e07",
//            "poId":"8e35f96a54a9aa2bcd413f46c4ca138c",
//            "poIsPay":true,
//            "poIsRefund":false,
//            "poIsSandbox":false,
//            "poIsVerify":true,
//            "poName":"LED会员",
//            "poPayTime":1629025448000,
//            "poPrice":9.90,
//            "poSourceChannel":"huawei",
//            "poSubject":"特价",
//            "poThreePartyUserId":"2088602275815820",
//            "poTransactionNo":"2021081522001415821415012183",
//            "poType":"ALIPAY",
//            "poUpdateTime":1629025448000,
//            "poVerifyTime":1629025448000
}
