package com.cn.shuangzi.bean;

import java.io.Serializable;

public class ActivelyPaymentInfo implements Serializable {

    class FixedPriceData  implements Serializable{
        private String fpTag;
        private String fpName;

        public String getFpTag() {
            return fpTag;
        }

        public String getFpName() {
            return fpName;
        }

        @Override
        public String toString() {
            return "FixedPriceData{" +
                    "fpTag='" + fpTag + '\'' +
                    ", fpName='" + fpName + '\'' +
                    '}';
        }
    }
//    {"fpTag":"GOLD_MEMBER","fpName":"连续包年"}
    private String appaType;
    private String appaPaymentPeriodType;
    private int appaPaymentPeriod;
    private double appaPaymentPrice;
    private FixedPriceData appaFixedPriceData;
    private String appaTripartiteAgreementNo;
    private String appaId;

    public String getMemberType() {
        if(appaFixedPriceData!=null) {
            return appaFixedPriceData.getFpTag();
        }
        return null;
    }

    public String getFpName() {
        if(appaFixedPriceData!=null) {
            return appaFixedPriceData.getFpName();
        }
        return null;
    }

    public String getAppaType() {
        return appaType;
    }

    public String getAppaPaymentPeriodType() {
        return appaPaymentPeriodType;
    }

    public int getAppaPaymentPeriod() {
        return appaPaymentPeriod;
    }

    public double getAppaPaymentPrice() {
        return appaPaymentPrice;
    }

    public FixedPriceData getAppaFixedPriceData() {
        return appaFixedPriceData;
    }

    public String getAppaTripartiteAgreementNo() {
        return appaTripartiteAgreementNo;
    }

    public String getAppaId() {
        return appaId;
    }

    @Override
    public String toString() {
        return "ActivelyPaymentInfo{" +
                "appaType='" + appaType + '\'' +
                ", appaPaymentPeriodType='" + appaPaymentPeriodType + '\'' +
                ", appaPaymentPeriod=" + appaPaymentPeriod +
                ", appaPaymentPrice=" + appaPaymentPrice +
                ", appaFixedPriceData=" + appaFixedPriceData +
                ", appaTripartiteAgreementNo='" + appaTripartiteAgreementNo + '\'' +
                ", appaId='" + appaId + '\'' +
                '}';
    }
}
