package com.cn.shuangzi;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.cn.shuangzi.activity.SZSplashActivity;
import com.cn.shuangzi.common.umeng.UMPushHelper;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.retrofit.SZRetrofitResponseListener;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.WebViewUtil;
import com.umeng.commonsdk.UMConfigure;

import java.util.Stack;

import androidx.multidex.MultiDexApplication;
import io.reactivex.Observable;


/**
 * Created by CN on 2016/10/31.
 */
public abstract class SZApp extends MultiDexApplication {
    protected static SZApp INSTANCE;
    private Stack<Activity> stackActivity;
    private Boolean isDebug = null;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        syncIsDebug(this);
        WebViewUtil.handleWebViewDir(this);
        if (isUMPushMode()) {
            UMPushHelper.preInit();
            SZManager.getInstance().init(this, isDebug(), false);
        } else {
            SZManager.getInstance().init(this, isDebug(), true);
        }
        if (SZUtil.isAgreePrivacy()) {
            init();
        }
    }

    public static SZApp getInstance() {
        return INSTANCE;
    }

    public void init() {
        if (!SZManager.getInstance().isInitUMDone()) {
            UMConfigure.submitPolicyGrantResult(this, true);
            if (isUMPushMode()) {
                UMPushHelper.init();
            } else {
                SZManager.getInstance().initUMConfig();
            }
            initInApplication();
        }
    }

    public boolean isDebug() {
        return isDebug == null ? false : isDebug.booleanValue();
    }

    private void syncIsDebug(Context context) {
        if (isDebug == null) {
            isDebug = context.getApplicationInfo() != null &&
                    (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        }
    }


    public Stack<Activity> getStackActivity() {
        if (stackActivity == null) {
            stackActivity = new Stack<>();
        }
        return stackActivity;
    }

    public Activity getTopActivity() {
        if (stackActivity != null && stackActivity.size() > 0) {
            return stackActivity.get(stackActivity.size() - 1);
        }
        return null;
    }

    public void addActivity(Activity activity) {
        if (stackActivity == null)
            stackActivity = new Stack<>();
        if (activity != null)
            stackActivity.add(activity);
    }

    public void removeActivity(Activity activity) {
        if (stackActivity != null && activity != null) {
            try {
                if(stackActivity.contains(activity)) {
                    stackActivity.remove(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void finishSplashActivity() {
        if (stackActivity != null) {
            for (Activity activity : stackActivity) {
                if (activity instanceof SZSplashActivity) {
                    activity.finish();
                    return;
                }
            }
        }
    }

    public void removeAndFinishAllActivity() {
        if (stackActivity != null) {
            for (Activity activity : stackActivity) {
                activity.finish();
            }
            stackActivity.clear();
        }
    }

    public int getActivityCount() {
        if (stackActivity != null) {
            return stackActivity.size();
        }
        return 0;
    }


    public <T extends Activity> void removeActivities(Class<T>... classActivities) {
        for (Activity activity : getStackActivity()) {
            for (Class classActivity : classActivities) {
                if (activity.getClass().getName().equals(classActivity.getName())) {
                    activity.finish();
                    break;
                }
            }
        }
    }

    public <T extends Activity> void removeActivitiesExcept(Class<T>... classActivities) {
        for (Activity activity : getStackActivity()) {
            boolean isFinish = true;
            for (Class classActivity : classActivities) {
                if (activity.getClass().getName().equals(classActivity.getName())) {
                    isFinish = false;
                    break;
                }
            }
            if (isFinish) {
                activity.finish();
            }
        }
    }

    public <T extends Activity> boolean isAliveActivity(Class<T> classActivity) {
        for (Activity activity : getStackActivity()) {
            if (activity.getClass().getName().equals(classActivity.getName())) {
                return true;
            }
        }
        return false;
    }

    public abstract Observable getSynchronizationUserInfoRequest();

    public void synchronizationUserInfo() {
        if (getSynchronizationUserInfoRequest() != null && !SZUtil.isUserInfoSynchronizationSuccess()) {
            SZRetrofitManager.getInstance().request(getSynchronizationUserInfoRequest(), null, new SZRetrofitResponseListener() {
                @Override
                public void onSuccess(String data) {
                    SZUtil.setSynchronizationUserInfo(true);
                }

                @Override
                public void onNetError(int errorCode, String errorMsg) {
                }

                @Override
                public void onWebServiceError(int errorCode, String errorId, String errorMsg) {
                }
            });
        }
    }

    public int getServiceErrorImgResId() {
        return 0;
    }

    public abstract String getSZAppId();

    public abstract String getUserToken();

    public abstract boolean isUMPushMode();

    public abstract void initInApplication();

    public abstract String getHuaWeiAppId();


}
