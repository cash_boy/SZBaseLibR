package com.cn.shuangzi.retrofit;


import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.util.SZUtil;

import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by CN on 2018-3-13.
 */

public class SZRetrofitManager {
    private static long TIMEOUT = SZConst.TIME_OUT;
    private static SZRetrofitManager INSTANCE;
    private static Map<String, List<Disposable>> disposableMap;
    private static Map<String, OkHttpClient> okHttpClientMap;
    private static Map<String, Retrofit> retrofitMap;
    private SZRequest szRequest;
    private String API_SERVICE = "https://switch.api.shuangzikeji.cn:8080";
    public static final int SUCCESS_CODE = 200;
    public static final int ERROR_EXCEPTION = 1000;
    public static final int ERROR_NET_FAILED = 1001;

    private SZRetrofitManager() {
        disposableMap = new HashMap<>();
        retrofitMap = new HashMap<>();
        okHttpClientMap = new HashMap<>();
    }

    public static SZRetrofitManager getInstance() {
        if (INSTANCE == null) {
            synchronized (SZRetrofitManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SZRetrofitManager();
                }
            }
        }
        return INSTANCE;
    }

    public Retrofit getRetrofit(String service) {
        return getRetrofit(service, null);
    }

    public Retrofit getRetrofit(String service, Interceptor interceptor) {
        return getRetrofit(service, null, interceptor);
    }

    public Retrofit getRetrofit(String service, String tag, Interceptor interceptor) {
        Retrofit retrofit = retrofitMap.get(tag != null ? service + tag : service);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(getOkInstance(service, tag, interceptor))//设置 HTTP Client  用于请求的连接
                    .addConverterFactory(ScalarsConverterFactory.create())   //字符串转换器
//                    .addConverterFactory(GsonConverterFactory.create())//json转换器
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(service)//请求的父目录
                    .build();
            retrofitMap.put(service, retrofit);
        }
        return retrofit;
    }

    public SZRequest getSZRequest() {
        if (szRequest == null) {
            szRequest = getRetrofit(API_SERVICE, new SZRequestInterceptor()).create(SZRequest.class);
        }
        return szRequest;
    }

    public void setAPI_SERVICE(String API_SERVICE) {
        this.API_SERVICE = API_SERVICE;
    }

    public static void setTIMEOUT(long TIMEOUT) {
        SZRetrofitManager.TIMEOUT = TIMEOUT;
    }

    public void request(Observable observable, String tag, SZRetrofitResponseListener responseListener) {
        request(observable, tag, responseListener, true);
    }

    public void request(Observable observable, final String tag, final SZRetrofitResponseListener responseListener, final boolean
            isNeedAnalyze) {
//                observable.subscribeOn(Schedulers.newThread())
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<String>() {
            private Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
                if (tag != null) {
                    List<Disposable> disposableList = disposableMap.get(tag);
                    if (disposableList == null) {
                        disposableList = new ArrayList<>();
                    }
                    disposableList.add(disposable);
                    disposableMap.put(tag, disposableList);
                }
            }

            @Override
            public void onNext(String result) {
                SZUtil.logError("response:" + result);
                try {
                    if (isNeedAnalyze) {
                        JSONObject jsonResult = new JSONObject(result);
                        int errorCode = jsonResult.getInt("statusCode");
                        if (SUCCESS_CODE == errorCode) {
                            if (responseListener != null) {
                                responseListener.onSuccess(jsonResult.getString("data"));
                            }
                        } else {
                            if (responseListener != null) {
                                String errorMsg = jsonResult.getString("errorMsg");
                                String errorId = null;
                                try {
                                    errorId = jsonResult.getString("errorId");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                responseListener.onWebServiceError(errorCode, errorId, errorMsg);
                            }
                        }
                    } else {
                        if (responseListener != null) {
                            responseListener.onSuccess(result);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (responseListener != null) {
                        responseListener.onNetError(ERROR_EXCEPTION, "code exception");
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (responseListener != null) {
                    responseListener.onNetError(ERROR_NET_FAILED, e.getMessage());
                }
                if (disposable != null && !disposable.isDisposed()) {
                    disposable.dispose();
                }
                if (tag != null) {
                    List<Disposable> disposableList = disposableMap.get(tag);
                    if (disposableList != null) {
                        disposableList.remove(disposable);
                    }
                }
            }

            @Override
            public void onComplete() {
                if (disposable != null && !disposable.isDisposed()) {
                    disposable.dispose();
                }
                if (tag != null) {
                    List<Disposable> disposableList = disposableMap.get(tag);
                    if (disposableList != null) {
                        disposableList.remove(disposable);
                    }
                }
            }
        });
    }

    //撤销请求响应
    public void cancelDisposable(String tag) {
        if (tag != null) {
            List<Disposable> disposableList = disposableMap.get(tag);
            if (disposableList != null) {
                for (Disposable disposable : disposableList) {
                    if (disposable != null && !disposable.isDisposed()) {
                        disposable.dispose();
                    }
                }
                disposableList.clear();
                disposableMap.remove(tag);
            }
        }
    }

    //撤销请求响应
    public void cancelAllDisposable() {
        if (!disposableMap.isEmpty()) {
            Iterator<String> iterator = disposableMap.keySet().iterator();
            if (iterator.hasNext()) {
                cancelDisposable(iterator.next());
            }
        }
        disposableMap.clear();
    }

    public OkHttpClient getOkInstance(String url, String tag, Interceptor interceptor) {
        if (okHttpClientMap == null) {
            okHttpClientMap = new HashMap<>();
        }
        OkHttpClient okHttpClient = okHttpClientMap.get(tag != null ? url + tag : url);
        if (okHttpClient == null) {
            synchronized (OkHttpClient.class) {
                if (okHttpClient == null) {
                    OkHttpClient.Builder builder;
                    try {
                        final TrustManager[] trustAllCerts = new TrustManager[]{
                                new X509TrustManager() {
                                    @Override
                                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                                    }

                                    @Override
                                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                                    }

                                    @Override
                                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                        return new java.security.cert.X509Certificate[]{};
                                    }
                                }
                        };
                        // Install the all-trusting trust manager
                        final SSLContext sslContext = SSLContext.getInstance("SSL");
                        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                        builder = new OkHttpClient.Builder().readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                                .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                                .sslSocketFactory(sslSocketFactory)
                                .hostnameVerifier(new HostnameVerifier() {
                                    @Override
                                    public boolean verify(String s, SSLSession sslSession) {
                                        return true;
                                    }
                                });
                        if (interceptor != null) {
                            builder.addInterceptor(interceptor);
                        } else {
                            builder.addInterceptor(new SZRequestInterceptor());
                        }
                        okHttpClient = builder.build();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (okHttpClient == null) {
                        builder = new OkHttpClient.Builder().readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                                .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS);
                        if (interceptor != null) {
                            builder.addInterceptor(interceptor);
                        } else {
                            builder.addInterceptor(new SZRequestInterceptor());
                        }
                        okHttpClient = builder.build();
                    }
                }
            }
        }
        return okHttpClient;
    }
}
