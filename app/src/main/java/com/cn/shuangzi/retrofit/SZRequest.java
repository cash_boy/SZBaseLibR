package com.cn.shuangzi.retrofit;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by CN.
 */

public interface SZRequest {

    //获取最后一条已购买会员的信息
    @POST("/SimpleMemberService/GetLastPayOrder")
    Observable<String> getLastVipOrder();

    //查询自动扣款合约
    @POST("/PayService/QueryProactivelyPaymentAgreement")
    @FormUrlEncoded
    Observable<String> getActivelyPaymentAgreement(@Field("consumerId") String consumerId);

    @POST("/UserService/CancellationAccount")
    @FormUrlEncoded
    Observable<String> cancellationAccount(@Field("consumerId") String consumerId,@Field("ticketId") String ticketId);

    //不包含会员信息
    @POST("/SimpleMemberService/QueryCancellationAccountInfo")
    Observable<String> getCancellationAccountInfo();

    @POST("/UserService/getAlipayAuthInfo")
    @FormUrlEncoded
    Observable<String> getAlipayAuthInfo(@Field("platformType") String platformType);

    @POST("/SimpleMemberService/GetTokenByCode")
    @FormUrlEncoded
    Observable<String> getTokenByCode(@Field("ticketId") String ticketId);

    @POST("/ApplicationService/InspectApplicationVersion")
    @FormUrlEncoded
    Observable<String> getNewVersionInfoV2(@Field("platform") String platform, @Field("version") int version);

    @POST("/UserService/ConsumeCode")
    @FormUrlEncoded
    Observable<String> loginThirdV2(@Field("platformType") String platformType, @Field("authCode") String authCode);

    @POST("/UserService/SendCaptcha")
    @FormUrlEncoded
    Observable<String> getVerificationCodeV2(@Field("phone") String phone);

    @POST("/UserService/ConsumeCaptcha")
    @FormUrlEncoded
    Observable<String> getTokenByVerificationCodeV2(@Field("phone") String phone, @Field("captcha") String captcha);

    @POST("/UserService/UpdateConsumerInfo")
    @FormUrlEncoded
    Observable<String> updateUserInfo(@Field("consumerId") String consumerId, @Field("userName") String userName, @Field("avatarUrl") String avatarUrl);

    @POST("/OpinionsService/GiveOpinions?platform=ANDROID")
    @FormUrlEncoded
    Observable<String> submitFeedbackV2(@Field("consumerId") String consumerId, @Field("opinions") String content
            , @Field("version") String version, @Field("contact") String contact);

//    @POST("/UserService/GetUserInfo")
//    @FormUrlEncoded
//    Observable<String> getUserInfo(@Field("ticketId") String ticketId);

    @POST("/UserService/GetSTSSign")
    Observable<String> getSTSSign();

    @POST("/PayService/QueryFixedPrice?platform=ANDROID")
    Observable<String> getVipPriceV2();

    @POST("/PayService/QueryFixedPrice?platform=ANDROID&saleVersion=200")
    Observable<String> getVipPriceV3();

    @POST("/PayService/SubmitOrder")
    @FormUrlEncoded
    Observable<String> submitOrderV2(@Field("consumerId") String consumerId,
                                   @Field("payType") String payType, @Field("deviceId") String deviceId, @Field("fixedPriceId") String fixedPriceId);

    @POST("/PayService/SubmitOrder")
    @FormUrlEncoded
    Observable<String> submitOrderV2(@Field("consumerId") String consumerId,
                                   @Field("payType") String payType, @Field("deviceId") String deviceId, @Field("fixedPriceId") String fixedPriceId,@Field("wxAppId") String wxAppId);


    @POST("/UserService/QueryConsumerTripartiteBinding")
    @FormUrlEncoded
    Observable<String> queryBindingInfo(@Field("consumerId") String consumerId);

    @POST("/UserService/BindingConsumerTripartite")
    @FormUrlEncoded
    Observable<String> bindThirdPlatformV2(@Field("consumerId") String consumerId
            , @Field("authCode") String authCode, @Field("platformType") String platformType);

    @POST("/UserService/UnBindingConsumerTripartite")
    @FormUrlEncoded
    Observable<String> unbindThirdPlatformV2(@Field("consumerId") String consumerId
            , @Field("platformType") String platformType);

    @POST("/UserService/QueryBindingConsumerPhoneAvailable")
    @FormUrlEncoded
    Observable<String> isPhoneCanBindV2(@Field("phone") String phone);

    @POST("/UserService/UpdateBindingConsumerPhone")
    @FormUrlEncoded
    Observable<String> updateBindingConsumerPhoneV2(@Field("consumerId") String consumerId, @Field("phone") String phone, @Field("newPhone") String newPhone
            , @Field("captcha") String code);


    @POST("/PushService/UpdateConfigPush")
    @FormUrlEncoded
    Observable<String> updateConfigPush(@Field("isClose") boolean isClose,@Field("type") String type);

    @POST("/PushService/GetConfigPushList")
    Observable<String> getPushConfigList();

    @GET("/LogicApplicationRewardPointsSign/QueryTodaySignStatus")
    Observable<String> queryTodaySignStatus();

}
