package com.cn.shuangzi;

import android.content.Context;

import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.util.Map;

/**
 * Created by CN.
 */

public class SZManager {
    private static SZManager INSTANCE;
    private boolean isDebugMode;
    private Context mContext;
    private String appKeyUM;
    private String appChannel;
    private String appMessageSecret;
    private boolean isInitUM;

    private SZManager() {
        isInitUM = false;
    }

    public static SZManager getInstance() {
        if (INSTANCE == null) {
            synchronized (SZManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SZManager();
                }
            }
        }
        return INSTANCE;
    }

    public void init(Context mContext) {
        this.init(mContext, false);
    }

    public void init(Context mContext, boolean isDebugMode) {
        this.init(mContext, isDebugMode, true);
    }

    public void init(Context mContext, boolean isDebugMode, boolean isPreInitUM) {
        this.mContext = mContext;
        this.isDebugMode = isDebugMode;
        SZToast.init(mContext);
        appKeyUM = SZUtil.getAppMetaDataString(mContext, "UMENG_APPKEY");
        appChannel = SZUtil.getAppMetaDataString(mContext, "UMENG_CHANNEL");
        appMessageSecret = SZUtil.getAppMetaDataString(mContext, "UMENG_MESSAGE_SECRET");
        if (isPreInitUM) {
            UMConfigure.preInit(mContext, appKeyUM, SZUtil.getChannel(mContext));
        }
    }

    public Context getContext() {
        return mContext;
    }

    public void cancelRequest(String tag) {
        if (tag != null) {
            SZRetrofitManager.getInstance().cancelDisposable(tag);
        } else {
            SZRetrofitManager.getInstance().cancelAllDisposable();
        }
    }

    public boolean isDebugMode() {
        return isDebugMode;
    }

    /**********************************************
     * 统计信息
     ***********************************************/
    public void initUMConfig() {
        if (isInitUM) {
            return;
        }
        isInitUM = true;

        UMConfigure.init(mContext, appKeyUM, appChannel, UMConfigure.DEVICE_TYPE_PHONE, appMessageSecret);
        MobclickAgent.setCatchUncaughtExceptions(true);
    }

    public String getAppChannel() {
        return appChannel;
    }

    public void onUMResume(Context context) {
        try {
            if (isUMInit()) {
                MobclickAgent.onResume(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMPause(Context context) {
        try {
            if (isUMInit()) {
                MobclickAgent.onPause(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMPageStart(String viewName) {
        try {
            if (isUMInit()) {
                MobclickAgent.onPageStart(viewName);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMPageEnd(String viewName) {
        try {
            if (isUMInit()) {
                MobclickAgent.onPageEnd(viewName);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMEvent(String eventID) {
        try {
            if (isUMInit()) {
                MobclickAgent.onEvent(mContext, eventID);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMEvent(String eventID, String label) {
        try {
            if (isUMInit()) {
                MobclickAgent.onEvent(mContext, eventID, label);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMEvent(String eventID, Map<String, String> map) {
        try {
            if (isUMInit()) {
                MobclickAgent.onEvent(mContext, eventID, map);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void onUMEventValue(String eventID, Map<String, String> map, int du) {
        try {
            if (isUMInit()) {
                MobclickAgent.onEventValue(mContext, eventID, map, du);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private boolean isUMInit() {
        if (appKeyUM == null || !isInitUMDone()) {
//            if (isDebugMode()) {
//                try {
//                    SZUtil.log("友盟未初始化");
//                    SZToast.error("友盟未初始化");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            return false;
//            throw new IllegalStateException("you doesn't init UMConfig!");
        }
        return true;
    }

    public boolean isInitUMDone() {
        return isInitUM;
    }

    public void setInitUMDone(boolean isInitDone) {
        isInitUM = isInitDone;
    }

}
