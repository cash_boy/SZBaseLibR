package com.cn.shuangzi.view.pop;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.pop.common.CtrlItem;
import com.cn.shuangzi.view.pop.common.PopItemCtrlAdp;

import java.util.List;


/**
 * Created by CN on 2017-5-22.
 */

public class FilterPop extends BasePop {
    private ListView lViFilter;
    private OnItemClickListener onItemClickListener;
    private PopItemCtrlAdp popItemCtrlAdp;

    public FilterPop(final Activity baseActivity, final List<CtrlItem> ctrlItemList, boolean isShowCheck) {
        this(baseActivity, ctrlItemList, true, true, isShowCheck);
    }

    public FilterPop(final Activity baseActivity, final List<CtrlItem> ctrlItemList, final boolean hasDarkBg, boolean isShowCheck) {
        this(baseActivity, ctrlItemList, hasDarkBg, true, isShowCheck);
    }

    public FilterPop(final Activity baseActivity, final List<CtrlItem> ctrlItemList, final boolean hasDarkBg, final boolean isResponseChecked, final boolean isShowCheck) {
        super(baseActivity, R.layout.pop_filter_list, hasDarkBg);
        getContentView().findViewById(R.id.viewBlack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissPop();
            }
        });
        lViFilter = getContentView().findViewById(R.id.lViFilter);
        if (ctrlItemList.size() > 5) {
            lViFilter.getLayoutParams().height = baseActivity.getResources().getDimensionPixelOffset(R.dimen
                    .item_height) * 5 + SZUtil.dip2px(10);
        }
        lViFilter.setTag(0);
        popItemCtrlAdp = new PopItemCtrlAdp(baseActivity, ctrlItemList);
        lViFilter.setAdapter(popItemCtrlAdp);
        lViFilter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position != (Integer) lViFilter.getTag()) {
                        if (isShowCheck) {
                            for (CtrlItem ctrlItem : ctrlItemList) {
                                ctrlItem.setChecked(false);
                            }
                            ctrlItemList.get(position).setChecked(true);
                        }
                        lViFilter.setTag(position);
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemChecked(position, ctrlItemList.get(position));
                        }
                    } else {
                        if (isResponseChecked) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onItemChecked(position, ctrlItemList.get(position));
                            }
                        }
                    }
                    dismissPop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void popAnim(final boolean isOpen) {
        if (isOpen) {
//            getContentView().findViewById(R.id.bg).setBackgroundColor(baseActivity.getResources().getColor(R.color.colorMask));
        } else {
            dismiss();
//            getContentView().findViewById(R.id.bg).setBackgroundColor(baseActivity.getResources().getColor(android.R.color.transparent));
            return;
        }
        lViFilter.post(new Runnable() {
            @Override
            public void run() {
                Animation animation;
                if (isOpen) {
                    notifySetChanged();
                    animation = new ScaleAnimation(1f, 1f,
                            0, 1);
                } else {
                    animation = new ScaleAnimation(1f, 1f,
                            1, 0);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            dismiss();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
                animation.setDuration(150);
                lViFilter.clearAnimation();
                lViFilter.setAnimation(animation);
                animation.start();
            }
        });
    }

    public void notifySetChanged() {
        if (popItemCtrlAdp != null) {
            popItemCtrlAdp.notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemChecked(int position, CtrlItem ctrlItem);
    }
}
