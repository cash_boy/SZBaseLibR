package com.cn.shuangzi.view.pop.common;

/**
 * Created by CN on 2017-5-22.
 */

public class CtrlItem {
    private String text;
    private Object value;
    private boolean isChecked;
    private int color;
    private Object extra;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value.toString();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public CtrlItem(String text) {
        this.text = text;
    }
    public CtrlItem(String text, Object value) {
        this.text = text;
        this.value = value;
    }
    public CtrlItem(String text, Object value, boolean isChecked) {
        this.text = text;
        this.value = value;
        this.isChecked = isChecked;
    }
    public CtrlItem(String text, Object value, int color, boolean isChecked) {
        this.text = text;
        this.value = value;
        this.color = color;
        this.isChecked = isChecked;
    }
    public CtrlItem(String text, String value, boolean isChecked, Object extra) {
        this.text = text;
        this.value = value;
        this.isChecked = isChecked;
        this.extra = extra;
    }
    public CtrlItem(String text, boolean isChecked, Object extra) {
        this.text = text;
        this.isChecked = isChecked;
        this.extra = extra;
    }
    public CtrlItem(String text, Object value, Object extra) {
        this.text = text;
        this.value = value;
        this.extra = extra;
    }

    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }

    @Override
    public String toString() {
        return "CtrlItem{" +
                "text='" + text + '\'' +
                ", value='" + value + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
