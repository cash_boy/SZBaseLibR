package com.cn.shuangzi.view;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZApp;

import androidx.annotation.ColorInt;

/**
 * Created by CN.
 */
public abstract class ColorClickableSpan extends ClickableSpan {
    @ColorInt
    private int color;

    public ColorClickableSpan() {
        this.color = SZApp.getInstance().getResources().getColor(R.color.txtKeyPointColor);
    }

    public ColorClickableSpan(int color) {
        this.color = color;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(color);
        ds.setUnderlineText(false);
    }

}
