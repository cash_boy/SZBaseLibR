package com.cn.shuangzi.view;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

/**
 * Created by CN.
 */
public abstract class FakeBoldClickableSpan extends ClickableSpan {
    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setFakeBoldText(true);
        ds.setUnderlineText(false);
    }
}
