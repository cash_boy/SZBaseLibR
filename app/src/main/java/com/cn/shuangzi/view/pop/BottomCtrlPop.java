package com.cn.shuangzi.view.pop;

import android.app.Activity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.pop.common.CtrlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CN.
 */

public class BottomCtrlPop extends BasePop{
    private LinearLayout lltCtrl;
    private TextView txtCancel;
    private List<CtrlItem> ctrlItemList;
    private FilterPop.OnItemClickListener onItemClickListener;
    private List<TextView> textViewList;
    private boolean isCheckMode;
    private String title;
    public BottomCtrlPop(Activity baseActivity, List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener) {
        this(baseActivity,ctrlItemList,onItemClickListener,false,null);
    }
    public BottomCtrlPop(Activity baseActivity, List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener, String title) {
        this(baseActivity,ctrlItemList,onItemClickListener,false,title);
    }
    public BottomCtrlPop(Activity baseActivity, List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener, boolean isCheckMode) {
        this(baseActivity,ctrlItemList,onItemClickListener,isCheckMode,null);
    }
    public BottomCtrlPop(Activity baseActivity, List<CtrlItem> ctrlItemList, FilterPop.OnItemClickListener onItemClickListener, boolean isCheckMode, String title) {
        super(baseActivity, R.layout.pop_bottom_ctrl, true);
        this.ctrlItemList = ctrlItemList;
        this.title = title;
        textViewList = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
        this.isCheckMode = isCheckMode;
        initView();
    }

    private void initView() {
        TextView txtTitle = getContentView().findViewById(R.id.txtTitle);
        View viewSpace = getContentView().findViewById(R.id.viewSpace);
        if(SZValidatorUtil.isValidString(title)){
            txtTitle.setVisibility(View.VISIBLE);
            txtTitle.setText(title);
            viewSpace.setVisibility(View.VISIBLE);
        }else {
            txtTitle.setVisibility(View.GONE);
            viewSpace.setVisibility(View.GONE);
        }
        int itemHeight = baseActivity.getResources().getDimensionPixelSize(R.dimen.actionBarSize);
//        int height = itemHeight;
        lltCtrl = getContentView().findViewById(R.id.lltCtrl);
        txtCancel = getContentView().findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissPop();
//                popAnimFromBottom(false);
            }
        });
        for (int i=0;i<ctrlItemList.size();i++){
            final CtrlItem ctrlItem = ctrlItemList.get(i);
            final int position = i;
            TextView textView = (TextView) LayoutInflater.from(baseActivity).inflate(R.layout.item_txt_bottom_pop,null);
            textView.setText(ctrlItem.getText());
//            textView.setBackgroundResource(R.drawable.bg_item);
            textView.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            textView.setHeight(itemHeight);
            if(isCheckMode) {
                textView.setTextColor(baseActivity.getResources().getColor(ctrlItem.isChecked() ? R.color.colorPrimary : R.color.txtColorDark));
            }
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,baseActivity.getResources().getDimension(R.dimen.txtSizeNormal));
            textView.setGravity(Gravity.CENTER);
            textViewList.add(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isCheckMode) {
                        resetCheckStatus();
                        ctrlItem.setChecked(true);
                        resetTextCtrlList();
                    }
                    dismissPop();
//                    popAnimFromBottom(false);
                    if(onItemClickListener!=null){
                        onItemClickListener.onItemChecked(position,ctrlItem);
                    }
                }
            });
            lltCtrl.addView(textView);
            if(i<ctrlItemList.size()-1) {
                lltCtrl.addView(getCutLine());
            }
//            height+=(itemHeight+1);
        }
//        setHeight(height);
    }
    private void resetCheckStatus(){
        for (CtrlItem ctrlItem : ctrlItemList){
            ctrlItem.setChecked(false);
        }
    }
    private View getCutLine(){
        View view = LayoutInflater.from(baseActivity).inflate(R.layout.bottom_pop_cut_line,null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,1));
        return view;
    }
    private void resetTextCtrlList(){
        for (int i=0;i<ctrlItemList.size();i++){
            CtrlItem ctrlItem = ctrlItemList.get(i);
            TextView textView = textViewList.get(i);
            textView.setText(ctrlItem.getText());
            if(isCheckMode) {
                textView.setTextColor(baseActivity.getResources().getColor(ctrlItem.isChecked() ? R.color.colorPrimary : R.color.txtColorDark));
            }
        }
    }
    public void setOnItemClickListener(FilterPop.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
