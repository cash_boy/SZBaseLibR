package com.cn.shuangzi.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ViewFlipper;


import com.cn.shuangzi.R;

import java.util.List;

/**
 * Created by CN.
 */
public class UpRollView extends ViewFlipper {
    private Context context;
    public UpRollView(Context context) {
        super(context);
        this.context = context;
        init(context);
    }
    private int Interval = 3000;

    public UpRollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        setFlipInterval(Interval);
    }
    public void setInterval(int i){
        Interval = i;
        setFlipInterval(Interval);//设置时间间隔
    }
    public void setAnim(){
        //设置时间间隔
        setInAnimation(context, R.anim.up_roll_in);
        setOutAnimation(context, R.anim.up_roll_out);
    }
    /**
     * 设置循环滚动的View数组
     */
    public void setViews(final List<View> views) {
        if (views == null || views.size() == 0) return;
        removeAllViews();
        for ( int i = 0; i < views.size(); i++) {
            final int position=i;
            //设置监听回调
            views.get(i).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position, views.get(position));
                    }
                }
            });
            addView(views.get(i));
        }
        startFlipping();
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }
}
