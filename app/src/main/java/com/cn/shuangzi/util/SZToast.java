package com.cn.shuangzi.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;

import androidx.annotation.CheckResult;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cn.shuangzi.R;


/**
 * @author CN
 * 在系统的Toast基础上封装
 */

@SuppressLint("InflateParams")
public class SZToast {

    @ColorInt
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#FFFFFF");

    @ColorInt
    private static final int ERROR_COLOR = Color.parseColor("#FD4C5B");

    @ColorInt
    private static final int INFO_COLOR = Color.parseColor("#3F51B5");

    @ColorInt
    private static final int SUCCESS_COLOR = Color.parseColor("#388E3C");

    @ColorInt
    private static final int WARNING_COLOR = Color.parseColor("#FFA900");

    private static final String TOAST_TYPEFACE = "sans-serif-condensed";

    private static Toast currentToast;

    //*******************************************普通 使用ApplicationContext 方法*********************
    /**
     * Toast 替代方法 ：立即显示无需等待
     */
    private static Toast mToast;
    private static long mExitTime;
    private static Context context;

    public static void init(Context context) {
        SZToast.context = context;
    }

    private static Context getContext() {
        return context;
    }

    public static void normal(int resId) {
        normal(getContext().getString(resId));
    }

    public static void normal(@NonNull String message) {
        try {
            normal(getContext(), message, Toast.LENGTH_SHORT, null, false).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void normalLong(@NonNull String message) {
        try {
            normal(getContext(), message, Toast.LENGTH_LONG, null, false).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToastLong(message);
        }
    }

    public static void normal(@NonNull String message, Drawable icon) {
        try {
            normal(getContext(), message, Toast.LENGTH_SHORT, icon, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void normal(@NonNull String message, int duration, Drawable icon) {
        try {
            normal(getContext(), message, duration, icon, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static Toast normal(@NonNull String message, int duration, Drawable icon, boolean withIcon) {
        return custom(getContext(), message, icon, DEFAULT_TEXT_COLOR, duration, withIcon);
    }

    public static void warning(int resId) {
        warning(getContext().getString(resId));
    }

    public static void warning(@NonNull String message) {
        try {
            warning(getContext(), message, Toast.LENGTH_SHORT, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void warningLong(@NonNull String message) {
        try {
            warning(getContext(), message, Toast.LENGTH_LONG, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToastLong(message);
        }
    }

    public static Toast warning(@NonNull String message, int duration, boolean withIcon) {
        return custom(getContext(), message, getDrawable(getContext(), R.drawable.ic_error_outline_white_48dp), DEFAULT_TEXT_COLOR, WARNING_COLOR, duration, withIcon, true);
    }

    public static void info(int resId) {
        info(getContext().getString(resId));
    }

    public static void info(@NonNull String message) {
        try {
            info(getContext(), message, Toast.LENGTH_SHORT, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void infoLong(@NonNull String message) {
        try {
            info(getContext(), message, Toast.LENGTH_LONG, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToastLong(message);
        }
    }


    public static Toast info(@NonNull String message, int duration, boolean withIcon) {
        return custom(getContext(), message, getDrawable(getContext(), R.drawable.ic_info_outline_white_48dp), DEFAULT_TEXT_COLOR, INFO_COLOR, duration, withIcon, true);
    }

    public static void success(int resId) {
        success(getContext().getString(resId));
    }

    public static void success(@NonNull String message) {
        try {
            success(getContext(), message, Toast.LENGTH_SHORT, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void successLong(@NonNull String message) {
        try {
            success(getContext(), message, Toast.LENGTH_LONG, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToastLong(message);
        }
    }

    public static Toast success(@NonNull String message, int duration, boolean withIcon) {
        return custom(getContext(), message, getDrawable(getContext(), R.drawable.ic_check_white_48dp), DEFAULT_TEXT_COLOR, SUCCESS_COLOR, duration, withIcon, true);
    }

    public static void error(int resId) {
        error(getContext().getString(resId));
    }

    public static void error(@NonNull String message) {
        try {
            error(getContext(), message, Toast.LENGTH_SHORT, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(message);
        }
    }

    public static void errorLong(@NonNull String message) {
        try {
            error(getContext(), message, Toast.LENGTH_LONG, true).show();
        } catch (Exception e) {
            e.printStackTrace();
            showToastLong(message);
        }
    }
    //===========================================使用ApplicationContext 方法=========================

    //*******************************************常规方法********************************************

    public static Toast error(@NonNull String message, int duration, boolean withIcon) {
        return custom(getContext(), message, getDrawable(getContext(), R.drawable.ic_clear_white_48dp), DEFAULT_TEXT_COLOR, ERROR_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast normal(@NonNull Context context, @NonNull String message) {
        return normal(context, message, Toast.LENGTH_SHORT, null, false);
    }

    @CheckResult
    public static Toast normal(@NonNull Context context, @NonNull String message, Drawable icon) {
        return normal(context, message, Toast.LENGTH_SHORT, icon, true);
    }

    @CheckResult
    public static Toast normal(@NonNull Context context, @NonNull String message, int duration) {
        return normal(context, message, duration, null, false);
    }

    @CheckResult
    public static Toast normal(@NonNull Context context, @NonNull String message, int duration, Drawable icon) {
        return normal(context, message, duration, icon, true);
    }

    @CheckResult
    public static Toast normal(@NonNull Context context, @NonNull String message, int duration, Drawable icon, boolean withIcon) {
        return custom(context, message, icon, DEFAULT_TEXT_COLOR, duration, withIcon);
    }

    @CheckResult
    public static Toast warning(@NonNull Context context, @NonNull String message) {
        return warning(context, message, Toast.LENGTH_SHORT, true);
    }

    @CheckResult
    public static Toast warning(@NonNull Context context, @NonNull String message, int duration) {
        return warning(context, message, duration, true);
    }

    @CheckResult
    public static Toast warning(@NonNull Context context, @NonNull String message, int duration, boolean withIcon) {
        return custom(context, message, getDrawable(context, R.drawable.ic_error_outline_white_48dp), DEFAULT_TEXT_COLOR, WARNING_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast info(@NonNull Context context, @NonNull String message) {
        return info(context, message, Toast.LENGTH_SHORT, true);
    }

    @CheckResult
    public static Toast info(@NonNull Context context, @NonNull String message, int duration) {
        return info(context, message, duration, true);
    }

    @CheckResult
    public static Toast info(@NonNull Context context, @NonNull String message, int duration, boolean withIcon) {
        return custom(context, message, getDrawable(context, R.drawable.ic_info_outline_white_48dp), DEFAULT_TEXT_COLOR, INFO_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast success(@NonNull Context context, @NonNull String message) {
        return success(context, message, Toast.LENGTH_SHORT, true);
    }

    @CheckResult
    public static Toast success(@NonNull Context context, @NonNull String message, int duration) {
        return success(context, message, duration, true);
    }

    @CheckResult
    public static Toast success(@NonNull Context context, @NonNull String message, int duration, boolean withIcon) {
        return custom(context, message, getDrawable(context, R.drawable.ic_check_white_48dp), DEFAULT_TEXT_COLOR, SUCCESS_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast error(@NonNull Context context, @NonNull String message) {
        return error(context, message, Toast.LENGTH_SHORT, true);
    }

    //===========================================常规方法============================================

    @CheckResult
    public static Toast error(@NonNull Context context, @NonNull String message, int duration) {
        return error(context, message, duration, true);
    }

    @CheckResult
    public static Toast error(@NonNull Context context, @NonNull String message, int duration, boolean withIcon) {
        return custom(context, message, getDrawable(context, R.drawable.ic_clear_white_48dp), DEFAULT_TEXT_COLOR, ERROR_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast custom(@NonNull Context context, @NonNull String message, Drawable icon, @ColorInt int textColor, int duration, boolean withIcon) {
        return custom(context, message, icon, textColor, -1, duration, withIcon, false);
    }

    //*******************************************内需方法********************************************

    @CheckResult
    public static Toast custom(@NonNull Context context, @NonNull String message, @DrawableRes int iconRes, @ColorInt int textColor, @ColorInt int tintColor, int duration, boolean withIcon, boolean shouldTint) {
        return custom(context, message, getDrawable(context, iconRes), textColor, tintColor, duration, withIcon, shouldTint);
    }

    @CheckResult
    public static Toast custom(@NonNull Context context, @NonNull String message, Drawable icon, @ColorInt int textColor, @ColorInt int tintColor, int duration, boolean withIcon, boolean shouldTint) {
        if (currentToast != null) {
            currentToast.cancel();
        }
        currentToast = new Toast(context);
        View toastLayout = null;
        boolean isSuccessCustomView = false;
        try {
            toastLayout = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.toast_layout, null);
            final ImageView toastIcon = toastLayout.findViewById(R.id.toast_icon);
            final TextView toastTextView = toastLayout.findViewById(R.id.toast_text);
            Drawable drawableFrame;

            if (shouldTint) {
                drawableFrame = tint9PatchDrawableFrame(context, tintColor);
            } else {
                drawableFrame = getDrawable(context, R.drawable.toast_frame);
            }
            setBackground(toastLayout, drawableFrame);

            if (withIcon) {
                if (icon == null) {
                    SZUtil.log("toast的icon不能为null");
                } else {
                    setBackground(toastIcon, icon);
                }
            } else {
                toastIcon.setVisibility(View.GONE);
            }

            toastTextView.setTextColor(textColor);
            toastTextView.setText(message);
            toastTextView.setTypeface(Typeface.create(TOAST_TYPEFACE, Typeface.NORMAL));
            isSuccessCustomView = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isSuccessCustomView && toastLayout != null) {
            currentToast.setView(toastLayout);
        } else {
            currentToast.setText(message);
        }
        currentToast.setDuration(duration);
        return currentToast;
    }

    public static final Drawable tint9PatchDrawableFrame(@NonNull Context context, @ColorInt int tintColor) {
        final NinePatchDrawable toastDrawable = (NinePatchDrawable) getDrawable(context, R.drawable.toast_frame);
        toastDrawable.setColorFilter(new PorterDuffColorFilter(tintColor, PorterDuff.Mode.SRC_IN));
        return toastDrawable;
    }
    //===========================================内需方法============================================


    //******************************************系统 Toast 替代方法***************************************

    public static final void setBackground(@NonNull View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static final Drawable getDrawable(@NonNull Context context, @DrawableRes int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }


    public static void showToastLong(String msg) {
        showToast(getContext(), msg, Toast.LENGTH_LONG);
    }

    public static void showToastLong(int resId) {
        showToast(getContext(), resId, Toast.LENGTH_LONG);
    }

    public static void showToast(String msg) {
        showToast(getContext(), msg, Toast.LENGTH_SHORT);
    }

    /**
     * Toast 替代方法 ：立即显示无需等待
     *
     * @param resId String资源ID
     */
    public static void showToast(int resId) {
        showToast(getContext(), resId, Toast.LENGTH_SHORT);
    }

    /**
     * Toast 替代方法 ：立即显示无需等待
     *
     * @param context  实体
     * @param resId    String资源ID
     * @param duration 显示时长
     */
    public static void showToast(Context context, int resId, int duration) {
        showToast(context, context.getString(resId), duration);
    }
    //===========================================Toast 替代方法======================================

    /**
     * Toast 替代方法 ：立即显示无需等待
     *
     * @param context  实体
     * @param msg      要显示的字符串
     * @param duration 显示时长
     */
    @SuppressLint("ShowToast")
    public static void showToast(Context context, String msg, int duration) {
        try {
            if (mToast != null) {
                mToast.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mToast = Toast.makeText(context, msg, duration);
            mToast.setText(msg);
            mToast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean doubleClickExit() {
        try {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                SZToast.normal("再按一次退出！");
                mExitTime = System.currentTimeMillis();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return true;
    }
}
