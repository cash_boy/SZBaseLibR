package com.cn.shuangzi.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.request.target.Target;
import com.cn.shuangzi.GlideApp;
import com.cn.shuangzi.GlideRequest;
import com.cn.shuangzi.R;

import androidx.fragment.app.FragmentActivity;

/**
 * 图片加载器
 * Created by CN on 2018/1/10.
 */
public class SZGlide {
    /**
     * Context 加载
     */
    public static void load(Context context, String url, ImageView imageView) {
        load(context, url, imageView, R.mipmap.ic_placeholder_light_gray);
    }

    public static void load(Context context, String url, ImageView imageView, int placeholder) {
        load(context, url, imageView, placeholder, 0, 0);
    }

    public static void load(Context context, String url, ImageView imageView, int width, int height) {
        load(context, url, imageView, R.mipmap.ic_placeholder_light_gray, width, height);
    }

    public static void load(Context context, String url, ImageView imageView, int placeholder, int width, int height) {
        if (imageView == null) {
            return;
        }
        GlideRequest glideRequest = GlideApp.with(context).load(url);
        if (placeholder != 0) {
            glideRequest.placeholder(placeholder);
        }
        if (width > 0 && height > 0) {
            glideRequest.override(width, height);
        } else {
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        }
        if (imageView.getScaleType() == null) {
            glideRequest.centerCrop();
        }
        glideRequest.into(imageView);
    }


    /**
     * Activity 加载
     */
    public static void load(Activity activity, String url, ImageView imageView) {
        load(activity, url, imageView, R.mipmap.ic_placeholder_light_gray);
    }

    public static void load(Activity activity, String url, ImageView imageView, int placeholder) {
        load(activity, url, imageView, placeholder, 0, 0);
    }

    public static void load(Activity activity, String url, ImageView imageView, int width, int height) {
        load(activity, url, imageView, R.mipmap.ic_placeholder_light_gray, width, height);
    }

    public static void load(Activity activity, String url, ImageView imageView, int placeholder, int width, int height) {
        if (imageView == null) {
            return;
        }
        GlideRequest glideRequest = GlideApp.with(activity).load(url);
        if (placeholder != 0) {
            glideRequest.placeholder(placeholder);
        }
        if (width > 0 && height > 0) {
            glideRequest.override(width, height);
        } else {
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        }
        if (imageView.getScaleType() == null) {
            glideRequest.centerCrop();
        }
        glideRequest.into(imageView);
    }


    /**
     * FragmentActivity 加载
     */
    public static void load(FragmentActivity fragmentActivity, String url, ImageView imageView) {
        load(fragmentActivity, url, imageView, R.mipmap.ic_placeholder_light_gray);
    }

    public static void load(FragmentActivity fragmentActivity, String url, ImageView imageView, int placeholder) {
        load(fragmentActivity, url, imageView, placeholder, 0, 0);
    }

    public static void load(FragmentActivity fragmentActivity, String url, ImageView imageView, int width, int height) {
        load(fragmentActivity, url, imageView, R.mipmap.ic_placeholder_light_gray, width, height);
    }

    public static void load(FragmentActivity fragmentActivity, String url, ImageView imageView, int placeholder, int width, int height) {
        if (imageView == null) {
            return;
        }
        GlideRequest glideRequest = GlideApp.with(fragmentActivity).load(url);
        if (placeholder != 0) {
            glideRequest.placeholder(placeholder);
        }
        if (width > 0 && height > 0) {
            glideRequest.override(width, height);
        } else {
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        }
        if (imageView.getScaleType() == null) {
            glideRequest.centerCrop();
        }
        glideRequest.into(imageView);
    }

    /**
     * androidx.fragment.app.Fragment 加载
     */
    public static void load(androidx.fragment.app.Fragment fragmentX, String url, ImageView imageView) {
        load(fragmentX, url, imageView, R.mipmap.ic_placeholder_light_gray);
    }

    public static void load(androidx.fragment.app.Fragment fragmentX, String url, ImageView imageView, int placeholder) {
        load(fragmentX, url, imageView, placeholder, 0, 0);
    }

    public static void load(androidx.fragment.app.Fragment fragmentX, String url, ImageView imageView, int width, int height) {
        load(fragmentX, url, imageView, R.mipmap.ic_placeholder_light_gray, width, height);
    }

    public static void load(androidx.fragment.app.Fragment fragmentX, String url, ImageView imageView, int placeholder, int width, int height) {
        if (imageView == null) {
            return;
        }
        GlideRequest glideRequest = GlideApp.with(fragmentX).load(url);
        if (placeholder != 0) {
            glideRequest.placeholder(placeholder);
        }
        if (width > 0 && height > 0) {
            glideRequest.override(width, height);
        } else {
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        }
        if (imageView.getScaleType() == null) {
            glideRequest.centerCrop();
        }
        glideRequest.into(imageView);
    }


    /**
     * Fragment 加载
     */
    public static void load(Fragment fragment, String url, ImageView imageView) {
        load(fragment, url, imageView, R.mipmap.ic_placeholder_light_gray);
    }

    public static void load(Fragment fragment, String url, ImageView imageView, int placeholder) {
        load(fragment, url, imageView, placeholder, 0, 0);
    }

    public static void load(Fragment fragment, String url, ImageView imageView, int width, int height) {
        load(fragment, url, imageView, R.mipmap.ic_placeholder_light_gray, width, height);
    }

    public static void load(Fragment fragment, String url, ImageView imageView, int placeholder, int width, int height) {
        if (imageView == null) {
            return;
        }
        GlideRequest glideRequest = GlideApp.with(fragment).load(url);
        if (placeholder != 0) {
            glideRequest.placeholder(placeholder);
        }
        if (width > 0 && height > 0) {
            glideRequest.override(width, height);
        } else {
            glideRequest.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        }
        if (imageView.getScaleType() == null) {
            glideRequest.centerCrop();
        }
        glideRequest.into(imageView);
    }
}
