package com.cn.shuangzi.util;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.userinfo.activity.SZBuyVipNewActivity;
import com.cn.shuangzi.view.AlertWidget;

/**
 * Created by CN.
 */
public class VipPrompt {
    public static AlertWidget showUseWarning(SZBaseActivity context, int resIdHeadTop, int resIdHeadBottom, String desc,
                                             int resIdVipPower, int resIdBtn,
                                             Class<? extends SZBuyVipNewActivity> vipClass) {
        return showUseWarning(context, resIdHeadTop, resIdHeadBottom, desc, resIdVipPower, resIdBtn, vipClass, null, null);
    }

    public static AlertWidget showUseError(final SZBaseActivity context, int resIdHeadTop, int resIdHeadBottom, String desc,
                                           int resIdVipPower, int resIdBtn,
                                           final Class<? extends SZBuyVipNewActivity> vipClass) {
        final AlertWidget alertWidget = new AlertWidget(context);
        alertWidget.setCanceledOnTouchOutside(false);
        alertWidget.show(R.layout.alert_use_warning);
        ImageView imgHeadTop = alertWidget.getWindow().findViewById(R.id.imgHeadTop);
        imgHeadTop.setImageResource(resIdHeadTop);
        RelativeLayout rltHeadBottom = alertWidget.getWindow().findViewById(R.id.rltHeadBottom);
        rltHeadBottom.setBackgroundResource(resIdHeadBottom);

        TextView txtDesc = alertWidget.getWindow().findViewById(R.id.txtDesc);
        txtDesc.setText(desc);
        ImageView imgVipPower = alertWidget.getWindow().findViewById(R.id.imgVipPower);
        imgVipPower.setImageResource(resIdVipPower);
        ImageView imgBtn = alertWidget.getWindow().findViewById(R.id.imgBtn);
        imgBtn.setImageResource(resIdBtn);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                context.startActivity(vipClass);
            }
        });
        final TextView txtBtn = alertWidget.getWindow().findViewById(R.id.txtBtn);
        txtBtn.setText("了解会员详情");
        txtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                context.startActivity(vipClass);
            }
        });
        return alertWidget;
    }

    public static AlertWidget showUseWarning(final SZBaseActivity context, int resIdHeadTop, int resIdHeadBottom, String desc,
                                             int resIdVipPower, int resIdBtn,
                                             final Class<? extends SZBuyVipNewActivity> vipClass, String bottomTxtBtn,
                                             final View.OnClickListener onBottomTxtBtnClickListener) {
        final AlertWidget alertWidget = new AlertWidget(context);
        alertWidget.setCanceledOnTouchOutside(false);
        alertWidget.show(R.layout.alert_use_warning);
        ImageView imgHeadTop = alertWidget.getWindow().findViewById(R.id.imgHeadTop);
        imgHeadTop.setImageResource(resIdHeadTop);
        RelativeLayout rltHeadBottom = alertWidget.getWindow().findViewById(R.id.rltHeadBottom);
        rltHeadBottom.setBackgroundResource(resIdHeadBottom);

        TextView txtDesc = alertWidget.getWindow().findViewById(R.id.txtDesc);
        txtDesc.setText(desc);
        ImageView imgVipPower = alertWidget.getWindow().findViewById(R.id.imgVipPower);
        imgVipPower.setImageResource(resIdVipPower);
        ImageView imgBtn = alertWidget.getWindow().findViewById(R.id.imgBtn);
        imgBtn.setImageResource(resIdBtn);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                context.startActivity(vipClass);
            }
        });
        final TextView txtBtn = alertWidget.getWindow().findViewById(R.id.txtBtn);
        if (SZValidatorUtil.isValidString(bottomTxtBtn)) {
            txtBtn.setText(bottomTxtBtn);
        }
        txtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                if (onBottomTxtBtnClickListener != null) {
                    onBottomTxtBtnClickListener.onClick(v);
                }
            }
        });
        return alertWidget;
    }

    public static AlertWidget showRemoveAdDialog(SZBaseActivity context, int resIdHeadTop, int resIdImgBtn1,
                                                 View.OnClickListener onBtn1ClickListener) {
        return showRemoveAdDialog(context, resIdHeadTop, resIdImgBtn1, onBtn1ClickListener, -1, null);
    }

    public static AlertWidget showRemoveAdDialog(SZBaseActivity context, int resIdHeadTop, int resIdImgBtn1,
                                                 final View.OnClickListener onBtn1ClickListener,
                                                 int resIdImgBtn2,
                                                 final View.OnClickListener onBtn2ClickListener) {
        final AlertWidget alertWidget = new AlertWidget(context);
        alertWidget.setCanceledOnTouchOutside(false);
        alertWidget.show(R.layout.alert_remove_ad);

        ImageView imgHeadTop = alertWidget.getWindow().findViewById(R.id.imgHeadTop);
        imgHeadTop.setImageResource(resIdHeadTop);

        ImageView imgBtn1 = alertWidget.getWindow().findViewById(R.id.imgBtn1);
        if (resIdImgBtn1 > 0) {
            imgBtn1.setImageResource(resIdImgBtn1);
            imgBtn1.setVisibility(View.VISIBLE);
            imgBtn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertWidget.close();
                    if (onBtn1ClickListener != null) {
                        onBtn1ClickListener.onClick(v);
                    }
                }
            });
        } else {
            imgBtn1.setVisibility(View.GONE);
        }
        ImageView imgBtn2 = alertWidget.getWindow().findViewById(R.id.imgBtn2);
        if (resIdImgBtn2 > 0) {
            imgBtn2.setImageResource(resIdImgBtn2);
            imgBtn2.setVisibility(View.VISIBLE);
            imgBtn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertWidget.close();
                    if (onBtn2ClickListener != null) {
                        onBtn2ClickListener.onClick(v);
                    }
                }
            });
        } else {
            imgBtn2.setVisibility(View.GONE);
        }
        TextView txtBtn = alertWidget.getWindow().findViewById(R.id.txtBtn);
        txtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
            }
        });
        return alertWidget;

    }
}
