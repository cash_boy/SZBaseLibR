package com.cn.shuangzi.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.view.AlertWidget;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;

import java.util.Date;

/**
 * Created by CN.
 */

public class FavourableCommentUtil {
    private static final String FAVOURABLE_COMMENT_SETTING = "FavourableComment";
    private static final String USED = "USED";
    private static final String USED_DATE = "USED_DATE";
    private static final String VERSION_SHOW_TIMES = "FC_TIMES";
    private static final String CLICK_COMMENT_TIME = "CLICK_COMMENT";
    private static final int YES = 2;
    private static final int NO = 1;
    private static final String CLICK_COMMENT_STATUS = "COMMENT_STATUS";
    private static final long ONE_DAY = 24 * 60 * 60 * 1000;
    private static int PER_VERSION_MAX_SHOW_TIMES = 1;
    private static int COMMENT_INTERVAL_DAY = 30;
    private static final SZXmlUtil xmlUtil = new SZXmlUtil(SZApp.getInstance(), FAVOURABLE_COMMENT_SETTING);

    public static void init(int per_version_max_show_times, int comment_interval_day) {
        PER_VERSION_MAX_SHOW_TIMES = per_version_max_show_times;
        COMMENT_INTERVAL_DAY = comment_interval_day;
    }

    public static String getShowTimesKey() {
        return VERSION_SHOW_TIMES + SZUtil.getVersionCode();
    }

    public static void setUsed() {
        if (!xmlUtil.getBoolean(USED)) {
            xmlUtil.put(USED, true);
            xmlUtil.put(USED_DATE, System.currentTimeMillis());
        }
    }

    public static long getUsedTime() {
        return xmlUtil.getLong(USED_DATE);
    }

    public static boolean isCanShowFavourableComment(boolean isShowAfterOneDay, boolean isForceShow) {
        if (isForceShow) {
            return true;
        }
        boolean isShow;
        if (isShowAfterOneDay) {
            isShow = xmlUtil.getInt(getShowTimesKey()) < PER_VERSION_MAX_SHOW_TIMES && xmlUtil.getBoolean(USED) && (System.currentTimeMillis() - getUsedTime() > SZDateUtil.ONE_DAY);
        } else {
            isShow = xmlUtil.getInt(getShowTimesKey()) < PER_VERSION_MAX_SHOW_TIMES && xmlUtil.getBoolean(USED);
        }
        if(isShow){
            long clickTime = xmlUtil.getLong(CLICK_COMMENT_TIME);
            if (clickTime > 0) {
                long spaceTime = System.currentTimeMillis() - clickTime;
                switch (xmlUtil.getInt(CLICK_COMMENT_STATUS)) {
                    case YES:
                        return spaceTime >= ONE_DAY * COMMENT_INTERVAL_DAY;
                    case NO:
                        return spaceTime >= ONE_DAY * COMMENT_INTERVAL_DAY;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean showFavourableComment(SZBaseActivity activity, Class<?> feedbackClass) {
        return showFavourableComment(activity, feedbackClass, null, null, null, true, false);
    }

    public static boolean showFavourableComment(SZBaseActivity activity, Class<?> feedbackClass, boolean isShowAfterOneDay, boolean isForceShow) {
        return showFavourableComment(activity, feedbackClass, null, null, null, isShowAfterOneDay, isForceShow);
    }

    /**
     * @param activity
     * @param feedbackClass
     * @param title
     * @param content
     * @param ok
     * @param isShowAfterOneDay      是否隔天才显示去好评
     * @param isForceShow 强制显示去好评
     * @return
     */
    public static boolean showFavourableComment(final SZBaseActivity activity, final Class<?> feedbackClass, String title, String content, String ok, boolean isShowAfterOneDay, boolean isForceShow) {
        if (isCanShowFavourableComment(isShowAfterOneDay, isForceShow)) {
            SweetAlertDialog sweetAlertDialog = activity.showAlert(SweetAlertDialog.NORMAL_TYPE, title != null ? title : "亲，赏个好评吧！", content != null ? content : "小的日夜努力写此应用，用过的小主赏个五星好评可好？",
                    ok != null ? ok : "赏好评", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            xmlUtil.put(CLICK_COMMENT_STATUS, YES);
                            xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
                            SZUtil.launchCurrentAppStoreForComment(activity);
                        }
                    }, "去吐槽", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            xmlUtil.put(CLICK_COMMENT_STATUS, YES);
                            xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
                            activity.startActivity(feedbackClass);
                        }
                    }, true, true, false);
            sweetAlertDialog.setCustomImage(R.drawable.ic_good_comment_alert);
            sweetAlertDialog.showCloseButton(true);
            xmlUtil.put(getShowTimesKey(), (xmlUtil.getInt(getShowTimesKey()) + 1));
            xmlUtil.put(CLICK_COMMENT_STATUS, NO);
            xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
//            final AlertWidget alertWidget = new AlertWidget(context);
//            alertWidget.setCanceledOnTouchOutside(false);
//            alertWidget.setCancelable(false);
//            alertWidget.setTitle(title!=null?title:"亲，赏个好评吧！");
//            alertWidget.setContent(content!=null?content:"小的日夜努力写此应用，用过的爷赏个五星好评可好？");
//            alertWidget.setOKListener(ok!=null?ok:"赏好评", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    alertWidget.close();
//                    xmlUtil.put(CLICK_COMMENT_STATUS, YES);
//                    xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
//                    SZUtil.launchCurrentAppStoreForComment(context);
//                }
//            });
//            alertWidget.setCancelListener(cancel!=null?cancel:"去吐槽", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    alertWidget.close();
//                    xmlUtil.put(CLICK_COMMENT_STATUS, YES);
//                    xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
//
//
//                    xmlUtil.put(CLICK_COMMENT_STATUS, NO);
//                    xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
//                }
//            });
//            alertWidget.show();
            return true;
        }
        return false;
    }
}
