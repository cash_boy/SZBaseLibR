package com.cn.shuangzi.userinfo.bean;

import android.graphics.pdf.PdfDocument;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class AccountAllBindInfo implements Serializable {
    private AccountBindInfo PHONE;
    private AccountBindInfo WECHAT;
    private AccountBindInfo WECHAT_2;

    public AccountBindInfo getWECHAT_2() {
        return WECHAT_2;
    }

    public AccountBindInfo getPHONE() {
        return PHONE;
    }

    public AccountBindInfo getWECHAT() {
        return WECHAT;
    }

    public void setPHONE(AccountBindInfo PHONE) {
        this.PHONE = PHONE;
    }

    public void setWECHAT(AccountBindInfo WECHAT) {
        this.WECHAT = WECHAT;
    }

    public void setWECHAT_2(AccountBindInfo WECHAT_2) {
        this.WECHAT_2 = WECHAT_2;
    }

    public boolean isBindPhone() {
        return PHONE != null && PHONE.isBound() && PHONE.getBoundName() != null;
    }

    public boolean isBindWeChat() {
        return WECHAT != null && WECHAT.isBound();
    }

    public boolean isBindWeChat2() {
        return WECHAT_2 != null && WECHAT_2.isBound();
    }

    @Override
    public String toString() {
        return "AccountAllBindInfo{" +
                "PHONE=" + PHONE +
                ", WECHAT=" + WECHAT +
                ", WECHAT_2=" + WECHAT_2 +
                '}';
    }
}
