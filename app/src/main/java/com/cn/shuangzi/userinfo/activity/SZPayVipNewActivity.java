package com.cn.shuangzi.userinfo.activity;

import android.app.Activity;
import android.graphics.Paint;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.SZBaseWebActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.bean.VipPriceInfo;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.loginplugin.common.socialized.ShareWeChat;
import com.cn.shuangzi.loginplugin.common.socialized.pay.PayManager;
import com.cn.shuangzi.loginplugin.common.socialized.pay.PayType;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZTextViewWriterUtil;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.util.UUID;

public abstract class SZPayVipNewActivity extends SZBaseActivity implements View.OnClickListener, SZInterfaceActivity {
    public TextView txtName;
    public TextView txtPrice;
    public TextView txtDesc;
    public Button btnPay;
    public ImageView imgCheckWechat;
    public ImageView imgCheckAlipay;
    public String payType = null;
    public VipPriceInfo vipPriceInfoCheck;
    private RelativeLayout rltAlipay;
    private RelativeLayout rltWechatPay;

    private CheckBox chkBoxAgree;
    private TextView txtVipProtocol;
    private TextView txtVipSkuNotes;
    private LinearLayout lltContinuePay;
    private TextView txtVipContinueProtocol;


    @Override
    protected int onGetChildView() {
        return R.layout.activity_pay_new;
    }

    @Override
    protected void onBindChildViews() {

        txtName = findViewById(R.id.txtName);
        txtPrice = findViewById(R.id.txtPrice);
        txtDesc = findViewById(R.id.txtDesc);
        btnPay = findViewById(R.id.btnPay);
        imgCheckWechat = findViewById(R.id.imgCheckWechat);
        imgCheckAlipay = findViewById(R.id.imgCheckAlipay);
        chkBoxAgree = findViewById(R.id.chkBoxAgree);
        rltAlipay = findViewById(R.id.rltAlipay);
        rltWechatPay = findViewById(R.id.rltWechatPay);

        chkBoxAgree = findViewById(R.id.chkBoxAgree);
        txtVipProtocol = findViewById(R.id.txtVipProtocol);
        txtVipSkuNotes = findViewById(R.id.txtVipSkuNotes);
        lltContinuePay = findViewById(R.id.lltContinuePay);
        txtVipContinueProtocol = findViewById(R.id.txtVipContinueProtocol);
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.lltContactService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareWeChat.getInstance().openCompanyService(
                        "ww319db112ce746335", "https://work.weixin.qq.com/kfid/kfc539223cd934af350"
                );
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("支付订单");
        vipPriceInfoCheck = getSerializableExtra();
        if (vipPriceInfoCheck == null) {
            finish();
            return;
        }
        SZTextViewWriterUtil.writeValue(txtName, vipPriceInfoCheck.getFpName());
        SZTextViewWriterUtil.writeValue(txtPrice, vipPriceInfoCheck.getFpPrice());
        SZTextViewWriterUtil.writeValueWithGone(txtDesc, vipPriceInfoCheck.getFpSubject());
        if (vipPriceInfoCheck.hasDeleteLine()) {
            txtDesc.setPaintFlags(txtDesc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        showPayType();
    }

    private void showPayType() {
        payType = null;
        if (vipPriceInfoCheck != null) {
            if (vipPriceInfoCheck.hasWechatPay()) {
                payType = SZConst.WXPAY;
                setPayTypeCheck();
                rltWechatPay.setVisibility(View.VISIBLE);
            } else {
                rltWechatPay.setVisibility(View.GONE);
            }
            if (vipPriceInfoCheck.hasAliPay()) {
                if (payType == null) {
                    payType = SZConst.ALIPAY;
                    setPayTypeCheck();
                }
                rltAlipay.setVisibility(View.VISIBLE);
            } else {
                rltAlipay.setVisibility(View.GONE);
            }
            /**支付SKU备注信息，目前用于显示持续订阅说明*/
            if (SZValidatorUtil.isValidString(vipPriceInfoCheck.getNotes())) {
                txtVipSkuNotes.setText(vipPriceInfoCheck.getNotes());
                txtVipSkuNotes.setVisibility(View.VISIBLE);
            } else {
                txtVipSkuNotes.setVisibility(View.GONE);
            }
            lltContinuePay.setVisibility(vipPriceInfoCheck.isSubscription() ? View.VISIBLE : View.GONE);
        } else {
            rltAlipay.setVisibility(View.GONE);
            rltWechatPay.setVisibility(View.GONE);
            lltContinuePay.setVisibility(View.GONE);
            txtVipSkuNotes.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void submitOrder() {
        btnPay.setEnabled(false);
        showBar(true, false);
        String uuid = null;
        try {
            uuid = UUID.randomUUID().toString().replace("-", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        request(SZRetrofitManager.getInstance().getSZRequest().submitOrderV2(getUserId(), payType, uuid, vipPriceInfoCheck.getFpId(), getWeChatAppId()), new SZCommonResponseListener() {
            @Override
            public void onResponseSuccess(String data) {
                btnPay.setEnabled(true);
                new PayManager(getActivity(), SZConst.ALIPAY.equals(payType) ? PayType.ALIPAY : PayType.WECHAT, data, new PayManager.OnPayListener() {
                    @Override
                    public void onPaySuccess() {
                        closeBar();
                        try {
                            for (Activity activity : SZApp.getInstance().getStackActivity()) {
                                if (activity instanceof SZBuyVipNewActivity) {
                                    activity.finish();
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(getSynchronizeVipClass());
                        finish();
                    }

                    @Override
                    public void onPayCancel() {
                        closeBar();
                        SZToast.warning("您取消了支付!");
                    }

                    @Override
                    public void onPayFailed() {
                        closeBar();
                        SZToast.error("支付失败,请重试!");
                    }
                }).toPay();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                btnPay.setEnabled(true);
                closeBar();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btnPay) {
            if (getUserId() == null) {
                startActivity(getLoginClass());
            } else {
                if (chkBoxAgree.isChecked()) {
                    submitOrder();
                } else {

                    if (lltContinuePay.getVisibility() == View.VISIBLE) {
                        SZUtil.showAgreeTwoProtocolAlert(getActivity(), "同意并支付", getString(R.string.vip_service_agreement_symbol),
                                getVipProtocolUrl(), "《自动续费服务说明》", getVipContinueProtocolUrl(), getWebViewActivity(), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        chkBoxAgree.setChecked(true);
                                        submitOrder();
                                    }
                                });
                    } else {
                        SZUtil.showAgreeProtocolAlert(this, "同意并支付", getString(R.string.vip_service_agreement_symbol),
                                getVipProtocolUrl(), getWebViewActivity(),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        chkBoxAgree.setChecked(true);
                                        submitOrder();
                                    }
                                });
                    }
//                    SZUtil.showAgreeProtocolAlert(this, "同意并购买", getString(R.string.vip_service_agreement_symbol),
//                            getVipProtocolUrl(), getWebViewActivity(),
//                            new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    chkBoxAgree.setChecked(true);
//                                    submitOrder();
//                                }
//                            });
                }
            }
        } else if (viewId == R.id.rltWechatPay) {
            payType = SZConst.WXPAY;
            setPayTypeCheck();
        } else if (viewId == R.id.rltAlipay) {
            payType = SZConst.ALIPAY;
            setPayTypeCheck();
        } else if (viewId == R.id.txtVipProtocol) {
            startActivity(getVipProtocolUrl(), getWebViewActivity());
        } else if (viewId == R.id.txtVipContinueProtocol) {
            startActivity(getVipContinueProtocolUrl(), getWebViewActivity());
        }
    }


    private void setPayTypeCheck() {
        if (payType != null) {
            switch (payType) {
                case SZConst.WXPAY:
                    imgCheckWechat.setImageResource(R.mipmap.pay_icon_choose);
                    imgCheckAlipay.setImageResource(R.mipmap.pay_icon_not);
                    break;
                case SZConst.ALIPAY:
                    imgCheckAlipay.setImageResource(R.mipmap.pay_icon_choose);
                    imgCheckWechat.setImageResource(R.mipmap.pay_icon_not);
                    break;
            }
        }
    }

    public abstract String getUserId();

    public abstract String getWeChatAppId();

    public abstract Class<?> getLoginClass();

    public abstract Class<?> getSynchronizeVipClass();

    public abstract String getVipProtocolUrl();

    protected abstract String getVipContinueProtocolUrl();

    public abstract Class<? extends SZBaseWebActivity> getWebViewActivity();
}
