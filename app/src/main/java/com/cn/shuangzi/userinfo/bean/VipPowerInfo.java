package com.cn.shuangzi.userinfo.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class VipPowerInfo implements Serializable{
    private String imgUrl;
    private int imgRes;
    private String powerName;
    private String powerDesc;
    private boolean isUrlMode;

    public VipPowerInfo() {
    }

    public VipPowerInfo(int imgRes, String powerName) {
        this.imgRes = imgRes;
        this.powerName = powerName;
        isUrlMode = false;
    }
    public VipPowerInfo(int imgRes, String powerName,String powerDesc) {
        this.imgRes = imgRes;
        this.powerName = powerName;
        this.powerDesc = powerDesc;
        isUrlMode = false;
    }

    public VipPowerInfo(String imgUrl, String powerName) {
        this.imgUrl = imgUrl;
        this.powerName = powerName;
        isUrlMode = true;
    }

    public VipPowerInfo(String imgUrl, int imgRes, String powerName, boolean isUrlMode) {
        this.imgUrl = imgUrl;
        this.imgRes = imgRes;
        this.powerName = powerName;
        this.isUrlMode = isUrlMode;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    public boolean isUrlMode() {
        return isUrlMode;
    }

    public void setUrlMode(boolean urlMode) {
        isUrlMode = urlMode;
    }

    public String getPowerDesc() {
        return powerDesc;
    }

    public void setPowerDesc(String powerDesc) {
        this.powerDesc = powerDesc;
    }
}
