package com.cn.shuangzi.userinfo.adp;

import android.content.Context;
import android.graphics.Color;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cn.shuangzi.R;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.bean.VipPriceInfo;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.util.List;

import androidx.annotation.Nullable;

public class VipNewAdp extends SZBaseAdp<VipPriceInfo> {
    public VipNewAdp(Context context, @Nullable List<VipPriceInfo> data) {
        super(context, R.layout.adp_vip_new, data);
    }

    @Override
    public void convertView(BaseViewHolder helper, VipPriceInfo item) {
        helper.setGone(R.id.viewSpace, helper.getLayoutPosition() > 0);
        if(SZValidatorUtil.isValidString(item.getDiscountDesc())){
            helper.setText(R.id.txtDiscountDesc,item.getDiscountDesc());
            helper.setGone(R.id.txtDiscountDesc,false);
        }else{
            helper.setGone(R.id.txtDiscountDesc,true);
        }
        helper.setGone(R.id.viewDescLine,!item.hasDeleteLine());
        helper.setText(R.id.txtName, item.getFpName());
        helper.setText(R.id.txtDesc, item.getFpSubject());
        helper.setText(R.id.txtPrice, item.getFpPrice());
        if (item.isCheck()) {
            helper.setBackgroundColor(R.id.viewDescLine,Color.parseColor("#FFFFFF"));
            helper.setTextColor(R.id.txtDesc, Color.parseColor("#FFFFFF"));
            helper.setTextColor(R.id.txtName, Color.parseColor("#7D4412"));
            helper.setBackgroundResource(R.id.lltBg, R.mipmap.vip_card_bg_c);
        } else {
            helper.setBackgroundColor(R.id.viewDescLine,Color.parseColor("#7D4412"));
            helper.setTextColor(R.id.txtDesc, Color.parseColor("#7D4412"));
            helper.setTextColor(R.id.txtName, Color.parseColor("#353535"));
            helper.setBackgroundResource(R.id.lltBg, R.mipmap.vip_card_bg_n);
        }
    }

}
