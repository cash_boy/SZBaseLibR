package com.cn.shuangzi.userinfo.activity;

import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by CN.
 */
public abstract class SZSynchronizePayActivity extends SZBaseActivity {
    public LinearLayout lltSynchronize;
    public ImageView imgWait;
    public TextView txtCountDown;
    public TextView txtSynchronizeDesc;
    public LinearLayout lltSuccess;
    public ImageView imgSuccess;
    public TextView txtSuccess;
    public LinearLayout lltSynchronizeError;
    public ImageView imgServiceError;
    public TextView txtServiceError;
    public TextView txtSynchronizeErrorBack;
    public TextView txtContactService;
    public LinearLayout lltNetError;
    public ImageView imgSynchronizeNetError;
    public TextView txtNetError;
    public TextView txtNetErrorBack;
    public TextView txtResynchronize;
    private int seconds;
    private Timer timer;
    private boolean isPayError;
    private boolean isNeedSynchronize;
    private Handler handler;
    private int synchronizeCount;

    @Override
    protected int onGetChildView() {
        return getLayoutResId();
    }

    @Override
    protected void onBindChildViews() {

        lltSynchronize = findViewById(R.id.lltSynchronize);
        imgWait = findViewById(R.id.imgWait);
        txtCountDown = findViewById(R.id.txtCountDown);
        txtSynchronizeDesc = findViewById(R.id.txtSynchronizeDesc);
        lltSuccess = findViewById(R.id.lltSuccess);
        imgSuccess = findViewById(R.id.imgSuccess);
        txtSuccess = findViewById(R.id.txtSuccess);
        lltSynchronizeError = findViewById(R.id.lltSynchronizeError);
        imgServiceError = findViewById(R.id.imgServiceError);
        txtServiceError = findViewById(R.id.txtServiceError);
        txtSynchronizeErrorBack = findViewById(R.id.txtSynchronizeErrorBack);
        txtContactService = findViewById(R.id.txtContactService);
        lltNetError = findViewById(R.id.lltNetError);
        imgSynchronizeNetError = findViewById(R.id.imgSynchronizeNetError);
        txtNetError = findViewById(R.id.txtNetError);
        txtNetErrorBack = findViewById(R.id.txtNetErrorBack);
        txtResynchronize = findViewById(R.id.txtResynchronize);

    }

    @Override
    protected void onBindChildListeners() {
        txtContactService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!onClickUserDefineServiceContactType()) {
                    showAlert(SweetAlertDialog.NORMAL_TYPE, "联系客服", getContactContent(),
                            "复制邮箱", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    SZUtil.copyClipboard(getActivity(), getContactEmail());
                                    SZToast.success("已复制邮箱到剪贴板！");
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            }, "取消", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            }, true);
                }
            }
        });
        txtResynchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNormalView();
                startTask();
                synchronizeDelay();
            }
        });
        txtNetErrorBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtSynchronizeErrorBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        if (getBackImgRes() != 0) {
            showBackImgLeft(getBackImgRes());
        }
        setTitleTxt("支付成功");
        initView();
        showNormalView();
        startTask();
        synchronizeDelay();
    }

    public void synchronizeDelay() {
        if (!isNeedSynchronize()) {
            return;
        }
        if (handler == null) {
            handler = new Handler();
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                addSynchronizeCount();
                synchronize();
            }
        }, 500);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    private void cancelTimer() {
        if (timer != null) {
            try {
                timer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            timer = null;
        }
    }

    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    seconds--;
                    txtCountDown.post(new Runnable() {
                        @Override
                        public void run() {
                            txtCountDown.setText(seconds + "s");
                        }
                    });
                    if (seconds <= 0) {
                        destroyTask();
                        txtCountDown.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isPayError()) {
                                    showSynchronizeError();
                                } else {
                                    showNetError();
                                }
                            }
                        });
                    }
                }
            }, 1000, 1000);
        }
    }

    public void startActivityDelayed(final String extra, final Class startActivityClass) {
        destroyTask();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (startActivityClass != null) {
                    startActivity(extra, startActivityClass);
                }
                finish();
            }
        }, getFinishDelayTime());
    }

    public void startActivityDelayed(final Serializable extra, final Class startActivityClass) {
        destroyTask();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (startActivityClass != null) {
                    startActivity(extra, startActivityClass);
                }
                finish();
            }
        }, getFinishDelayTime());
    }

    public void startActivityDelayed(final Class startActivityClass) {
        destroyTask();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (startActivityClass != null) {
                    startActivity(startActivityClass);
                }
                finish();
            }
        }, getFinishDelayTime());
    }

    public void showSuccessView() {
        lltSynchronize.setVisibility(View.GONE);
        lltSynchronizeError.setVisibility(View.GONE);
        lltNetError.setVisibility(View.GONE);
        lltSuccess.setVisibility(View.VISIBLE);
    }

    public void showNormalView() {
        seconds = getSeconds();
        txtCountDown.setText(seconds + "s");
        lltSynchronize.setVisibility(View.VISIBLE);
        lltSynchronizeError.setVisibility(View.GONE);
        lltNetError.setVisibility(View.GONE);
        lltSuccess.setVisibility(View.GONE);
    }

    public void showNetError() {
        lltSynchronize.setVisibility(View.GONE);
        lltSynchronizeError.setVisibility(View.GONE);
        lltNetError.setVisibility(View.VISIBLE);
        lltSuccess.setVisibility(View.GONE);
    }

    public void showSynchronizeError() {
        lltSynchronize.setVisibility(View.GONE);
        lltSynchronizeError.setVisibility(View.VISIBLE);
        lltNetError.setVisibility(View.GONE);
        lltSuccess.setVisibility(View.GONE);
    }

    public int getSynchronizeCount() {
        return synchronizeCount;
    }

    public void addSynchronizeCount() {
        synchronizeCount++;
    }

    public void resetSynchronizeCount() {
        synchronizeCount = 0;
    }

    public boolean isPayError() {
        return isPayError;
    }

    public void setPayError(boolean payError) {
        isPayError = payError;
    }

    public void setPayErrorMinCountCondition() {
        if (getSynchronizeCount() > getMinSynchronizeCount()) {
            setPayError(true);
        }
    }

    private boolean isNeedSynchronize() {
        return isNeedSynchronize;
    }

    private void setNeedSynchronize(boolean needSynchronize) {
        isNeedSynchronize = needSynchronize;
    }

    public void startTask() {
        setPayError(false);
        resetSynchronizeCount();
        setNeedSynchronize(true);
        startTimer();
    }

    public void destroyTask() {
        setNeedSynchronize(false);
        cancelTimer();
    }

    public void setText(String desc, String serviceError, String netError) {
        if (desc != null) {
            txtSynchronizeDesc.setText(desc);
        }
        if (netError != null) {
            txtNetError.setText(netError);
        }
        if (serviceError != null) {
            txtServiceError.setText(serviceError);
        }
    }

    public void setImage(int imgNetErrorRes, int imgServiceErrorRes, int imgSuccessRes, int imgWaitRes) {
        if (imgServiceErrorRes != 0) {
            imgServiceError.setImageResource(imgServiceErrorRes);
        }
        if (imgNetErrorRes != 0) {
            imgSynchronizeNetError.setImageResource(imgNetErrorRes);
        }
        if (imgSuccessRes != 0) {
            imgSuccess.setImageResource(imgSuccessRes);
        }
        if (imgWaitRes != 0) {
            imgWait.setImageResource(imgWaitRes);
        }
    }

    @Override
    public void onBackPressed() {
        if (isFinishWithUserPressBackBtn()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyTask();
    }

    public int getSeconds() {
        return 60;
    }

    public int getFinishDelayTime() {
        return 500;
    }

    public int getMinSynchronizeCount() {
        return 1;
    }

    protected String getContactContent() {
        return "1.应用名称：" + getString(R.string.app_name) + "\n2.反馈内容：XXXXXXXXXXX\n\n请按以上格式编辑邮件，发送至" + getContactEmail() + "，稍后会有客服进行回复。";
    }

    public boolean isFinishWithUserPressBackBtn() {
        return false;
    }

    public int getLayoutResId() {
        return R.layout.activity_synchronize_pay;
    }

    public void initView() {
    }

    public abstract boolean onClickUserDefineServiceContactType();

    public abstract int getBackImgRes();

    public abstract String getContactEmail();

    public abstract void synchronize();

}
