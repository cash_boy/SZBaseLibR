package com.cn.shuangzi.userinfo.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.view.ClearEditText;

public abstract class SZCheckBindPhoneActivity extends SZBaseActivity implements SZInterfaceActivity {
    public ClearEditText edtPhone;
    public Button btnNext;
    public String oldPhone;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_check_bind_phone;
    }

    @Override
    protected void onBindChildViews() {
        edtPhone = findViewById(R.id.edtPhone);
        btnNext = findViewById(R.id.btnNext);
    }

    @Override
    protected void onBindChildListeners() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    public void next() {
        btnNext.setEnabled(false);
        String phone = edtPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            SZToast.warning("手机号不能为空！");
            btnNext.setEnabled(true);
        } else {
            if (phone.equals(oldPhone)) {
                startActivity(phone, getChangePoneActivity());
                finish();
            } else {
                SZToast.warning("原手机号码错误！");
                btnNext.setEnabled(true);
            }
        }
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        oldPhone = getStringExtra();
        if (oldPhone == null) {
            finish();
            return;
        }
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("更换手机");
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    public abstract Class getChangePoneActivity();
}
