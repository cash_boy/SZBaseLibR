package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.shape_imgview.CustomShapeImageView;


public abstract class SZAboutUsActivity extends SZBaseActivity implements SZInterfaceActivity {
    public CustomShapeImageView imgLogo;
    public TextView txtAppName;
    public TextView txtVersion;
    public TextView txtBeiAn;
    public TextView txtCompanyName;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_about_us;
    }

    @Override
    protected void onBindChildViews() {
        imgLogo = findViewById(R.id.imgLogo);
        txtAppName = findViewById(R.id.txtAppName);
        txtCompanyName = findViewById(R.id.txtCompanyName);
        txtVersion = findViewById(R.id.txtVersion);
        txtBeiAn = findViewById(R.id.txtBeiAn);
    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(R.string.txt_about_us);
        imgLogo.setImageResource(getLogoImg());
        txtAppName.setText(getAppName());
        txtCompanyName.setText(getCompanyName() != 0 ? getCompanyName() : R.string.txt_company_name);
        txtVersion.setText(getString(R.string.txt_version)+SZUtil.getVersionName());
        String benAnInfo = getBeiAnInfo();
        if(SZValidatorUtil.isValidString(benAnInfo)) {
            txtBeiAn.setText("APP备案编号："+benAnInfo);
            txtBeiAn.setVisibility(View.VISIBLE);
            txtBeiAn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SZUtil.openSystemWeb(getActivity(),"https://beian.miit.gov.cn");
                }
            });
        }else{
            txtBeiAn.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    public abstract int getLogoImg();

    public abstract int getAppName();

    public abstract int getCompanyName();

    public abstract String getBeiAnInfo();

}
