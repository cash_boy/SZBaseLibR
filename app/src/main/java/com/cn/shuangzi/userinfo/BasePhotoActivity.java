package com.cn.shuangzi.userinfo;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.cn.shuangzi.R;
import com.cn.shuangzi.userinfo.permission.RequestCameraPermissionHelper;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.permission.RequestPermissionViewP;
import com.cn.shuangzi.userinfo.permission.RequestWritePermissionHelper;
import com.cn.shuangzi.util.SZFileUtil;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.pop.FilterPop;
import com.cn.shuangzi.view.pop.common.CtrlItem;

import org.devio.takephoto.app.TakePhoto;
import org.devio.takephoto.app.TakePhotoImpl;
import org.devio.takephoto.compress.CompressConfig;
import org.devio.takephoto.model.CropOptions;
import org.devio.takephoto.model.InvokeParam;
import org.devio.takephoto.model.LubanOptions;
import org.devio.takephoto.model.TContextWrap;
import org.devio.takephoto.model.TResult;
import org.devio.takephoto.permission.InvokeListener;
import org.devio.takephoto.permission.PermissionManager;
import org.devio.takephoto.permission.TakePhotoInvocationHandler;
import org.devio.takephoto.uitl.TUriParse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class BasePhotoActivity extends SZBaseActivity implements TakePhoto.TakeResultListener, InvokeListener {

    private File fileLuBan;
    private InvokeParam invokeParam;
    private TakePhoto takePhoto;

    private boolean isNeedCrop;
    private List<CtrlItem> ctrlItemList;

    public abstract void onTakePhotoSuccess(TResult result);

    public boolean isCropAspect() {
        return true;
    }

    public boolean isNeedClearTmpImg() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showNormalPop(final boolean isNeedCrop, final boolean isNeedTitle, final boolean isNeedReset) {
        if (ctrlItemList == null) {
            ctrlItemList = new ArrayList<>();
            ctrlItemList.add(new CtrlItem(getString(R.string.txt_take_capture)));
            ctrlItemList.add(new CtrlItem(getString(R.string.txt_pick_from_gallery)));
        }
        showBottomCtrl(ctrlItemList, new FilterPop.OnItemClickListener() {
            @Override
            public void onItemChecked(final int position, CtrlItem ctrlItem) {
                boolean isGallery = 1 == position;
                final boolean hasPermission = SZUtil.hasPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE);
                requestPermission(getString(isGallery?R.string.txt_request_write_permission_avatar:R.string.txt_request_camera_and_write_permission_avatar),null,new RequestWritePermissionHelper(getActivity(), new RequestPermissionViewP() {
                    @Override
                    public void onPermissionRequestSuccess(String[] permissionName) {
                        switch (position) {
                            case 0:
                                if (isNeedCrop) {
                                    pickFromCaptureCrop(hasPermission);
                                } else {
                                    pickFromCapture(hasPermission);
                                }
                                break;
                            case 1:
                                if (isNeedCrop) {
                                    pickFromGalleryCrop();
                                } else {
                                    pickFromGallery();
                                }
                                break;
                        }
                    }

                    @Override
                    public void onRequestPermissionAlertCancelled(String[] permissionName) {
                        SZToast.error("允许媒体文件权限后，才能保存图片哦~");
                    }
                }),isGallery);
            }
        }, isNeedTitle ? getString(R.string.txt_select_pic) : null, isNeedReset);
    }

    @Override
    public void takeSuccess(final TResult result) {
        //这里不是主线程
        fileLuBan = new File(result.getImage().getCompressPath());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onTakePhotoSuccess(result);
            }
        });
    }

    @Override
    public void takeFail(TResult result, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SZToast.error(msg);
            }
        });
    }

    @Override
    public void takeCancel() {
    }

    public void pickFromGalleryWithPermission() {
        requestPermission(getString(R.string.txt_request_write_permission_avatar),new RequestWritePermissionHelper(getActivity(),  new RequestPermissionViewP() {
            @Override
            public void onPermissionRequestSuccess(String[] permissionName) {
                isNeedCrop = false;
                configCompress(getTakePhoto());
                getTakePhoto().onPickFromGallery();
            }

            @Override
            public void onRequestPermissionAlertCancelled(String[] permissionName) {
                SZToast.error("允许媒体文件权限后，才能保存图片哦~");
            }
        }));
    }

    public void pickFromGalleryCropWithPermission() {
        requestPermission(getString(R.string.txt_request_write_permission_avatar),new RequestWritePermissionHelper(getActivity(),  new RequestPermissionViewP() {
            @Override
            public void onPermissionRequestSuccess(String[] permissionName) {
                SZFileUtil.resetInstance();
                isNeedCrop = true;
                configCompress(getTakePhoto());
                Uri imageUri = TUriParse.getUriFileByVersion(getActivity(),SZFileUtil.getInstance().getOneTmpPic());
                getTakePhoto().onPickFromGalleryWithCrop(imageUri, getCropOptions());
            }

            @Override
            public void onRequestPermissionAlertCancelled(String[] permissionName) {
                SZToast.error("允许媒体文件权限后，才能保存图片哦~");
            }
        }));
    }

    private void pickFromGallery() {
        isNeedCrop = false;
        configCompress(getTakePhoto());
        getTakePhoto().onPickFromGallery();
    }

    private void pickFromGalleryCrop() {
        isNeedCrop = true;
        configCompress(getTakePhoto());
        Uri imageUri = TUriParse.getUriFileByVersion(getActivity(),SZFileUtil.getInstance().getOneTmpPic());
        getTakePhoto().onPickFromGalleryWithCrop(imageUri, getCropOptions());
    }

    private void requestCapturePermission(boolean isNeedAlertPermissionDesc) {
        if(isNeedAlertPermissionDesc) {
            boolean hasWritePermission = SZUtil.hasPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE);
            requestPermission(hasWritePermission?getString(R.string.txt_request_camera_permission_avatar):getString(R.string.txt_request_camera_and_write_permission_avatar), new RequestCameraPermissionHelper(this, new RequestPermissionViewP() {
                @Override
                public void onPermissionRequestSuccess(String[] permissionName) {
                    for (String permission : permissionName) {
                        if (Manifest.permission.CAMERA.equalsIgnoreCase(permission)) {
                            configCompress(getTakePhoto());

                            Uri imageUri = TUriParse.getUriFileByVersion(getActivity(), SZFileUtil.getInstance().getOneTmpPic());
                            if (isNeedCrop) {
                                getTakePhoto().onPickFromCaptureWithCrop(imageUri, getCropOptions());
                            } else {
                                getTakePhoto().onPickFromCapture(imageUri);
                            }
                        }
                    }
                }

                @Override
                public void onRequestPermissionAlertCancelled(String[] permissionName) {
                    SZToast.error("相机摄像头权限获取失败！");
                }
            }));
        }else{
            requestPermissionWithoutDesc(new RequestCameraPermissionHelper(this, new RequestPermissionViewP() {
                @Override
                public void onPermissionRequestSuccess(String[] permissionName) {
                    for (String permission : permissionName) {
                        if (Manifest.permission.CAMERA.equalsIgnoreCase(permission)) {
                            configCompress(getTakePhoto());

                            Uri imageUri = TUriParse.getUriFileByVersion(getActivity(), SZFileUtil.getInstance().getOneTmpPic());
                            if (isNeedCrop) {
                                getTakePhoto().onPickFromCaptureWithCrop(imageUri, getCropOptions());
                            } else {
                                getTakePhoto().onPickFromCapture(imageUri);
                            }
                        }
                    }
                }

                @Override
                public void onRequestPermissionAlertCancelled(String[] permissionName) {
                    SZToast.error("相机摄像头权限获取失败！");
                }
            }));
        }
    }

    private void pickFromCapture(boolean isNeedAlertPermissionDesc) {
        isNeedCrop = false;
        requestCapturePermission(isNeedAlertPermissionDesc);
    }

    private void pickFromCaptureCrop(boolean isNeedAlertPermissionDesc) {
        isNeedCrop = true;
        requestCapturePermission(isNeedAlertPermissionDesc);
    }

    public CropOptions getCropOptions() {
        return getCropOptions(isCropAspect());
    }

    public CropOptions getCropOptions(boolean isAspect) {
        int height = 800;
        int width = 800;
        CropOptions.Builder builder = new CropOptions.Builder();
        if (isAspect) {
            builder.setAspectX(width).setAspectY(height);//按照比例拉伸
        } else {
            builder.setOutputX(width).setOutputY(height);//长宽 自由拉伸
        }
        builder.setWithOwnCrop(false);
        return builder.create();
    }

    public CropOptions getCropOptions(boolean isAspect, int height, int width) {
        CropOptions.Builder builder = new CropOptions.Builder();
        if (isAspect) {
            builder.setAspectX(width).setAspectY(height);//按照比例拉伸
        } else {
            builder.setOutputX(width).setOutputY(height);//长宽 自由拉伸
        }
        builder.setWithOwnCrop(false);
        return builder.create();
    }

    /**
     * 获取TakePhoto实例
     *
     * @return
     */
    public TakePhoto getTakePhoto() {
        if (takePhoto == null) {
            takePhoto = (TakePhoto) TakePhotoInvocationHandler.of(this).bind(new TakePhotoImpl(this, this));
        }
        return takePhoto;
    }

    @Override
    public PermissionManager.TPermissionType invoke(InvokeParam invokeParam) {
        PermissionManager.TPermissionType type = PermissionManager.checkPermission(TContextWrap.of(this), invokeParam.getMethod());
        if (PermissionManager.TPermissionType.WAIT.equals(type)) {
            this.invokeParam = invokeParam;
        }
        return type;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        getTakePhoto().onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.TPermissionType type = PermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handlePermissionsResult(this, type, invokeParam, this);
    }


    private void configCompress(TakePhoto takePhoto) {
        LubanOptions option = new LubanOptions.Builder().setMaxSize(1024 * 200).create();
        CompressConfig config = CompressConfig.ofLuban(option);
        config.enablePixelCompress(false);
        takePhoto.onEnableCompress(config, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isNeedClearTmpImg()) {
            try {
                SZFileUtil.getInstance().clearTmpFiles();
            } catch (Exception e) {
            }
            try {
                if (fileLuBan != null && fileLuBan.getAbsolutePath().contains("luban_disk_cache")) {
                    SZFileUtil.getInstance().clearFile(fileLuBan.getParentFile());
                }
            } catch (Exception e) {
            }
        }
    }
}
