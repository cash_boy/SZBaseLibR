package com.cn.shuangzi.userinfo.activity;

import android.view.View;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;


public abstract class SZCancellationAccountSuccessActivity extends SZBaseActivity implements SZInterfaceActivity{
    @Override
    protected int onGetChildView() {
        return R.layout.activity_cancellation_account_success;
    }

    @Override
    protected void onBindChildViews() {
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.btnLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogoutClick();
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        setTitleTxt("帐号注销");
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    @Override
    public int getBackImgLeft() {
        return 0;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public abstract void onLogoutClick();

}
