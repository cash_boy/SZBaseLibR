package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.bean.VersionInfo;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.common.VersionUpdate;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.common.UserManager;
import com.cn.shuangzi.util.SZFileUtil;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZXmlUtil;
import com.cn.shuangzi.view.AlertWidget;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;
import com.google.gson.Gson;

/**
 * Created by CN on 2018-1-25.
 */

public abstract class SZSettingActivity extends SZBaseActivity implements SZInterfaceActivity {
    public Button btnLogout;
    public TextView txtUpdate;
    public TextView txtVersion;
    public TextView txtCache;
    public View rltClearCache;
    public View lltAccountSafe;
    public View lltUserAgreement;
    public View lltPrivacy;
    public View lltCancelPrivacy;
    public View lltUpdate;
    public View lltRecommendAd;
    public CompoundButton switchRecommendAd;
    public SZXmlUtil xmlUtil;
    private boolean hasNewVersion = true;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_setting;
    }

    @Override
    protected void onBindChildViews() {
        btnLogout = findViewById(R.id.btnLogout);
        txtCache = findViewById(R.id.txtCache);
        txtUpdate = findViewById(R.id.txtUpdate);
        txtVersion = findViewById(R.id.txtVersion);
        rltClearCache = findViewById(R.id.rltClearCache);
        lltAccountSafe = findViewById(R.id.lltAccountSafe);
        lltUserAgreement = findViewById(R.id.lltUserAgreement);
        lltPrivacy = findViewById(R.id.lltPrivacy);
        lltCancelPrivacy = findViewById(R.id.lltCancelPrivacy);
        lltUpdate = findViewById(R.id.lltUpdate);
        lltRecommendAd = findViewById(R.id.lltRecommendAd);
        switchRecommendAd = findViewById(R.id.switchRecommendAd);
    }

    @Override
    protected void onBindChildListeners() {
        rltClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCache();
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        lltAccountSafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(isUserLogin() ? getAccountSafeClass() : getLoginClass());
            }
        });
        lltCancelPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelPrivacy();
            }
        });
        lltUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hasNewVersion) {
                    requestNewVersion();
                }
            }
        });
        if(lltPrivacy!=null) {
            lltPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickPrivacyUrl();
                }
            });
        }
        if(lltUserAgreement!=null) {
            lltUserAgreement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickUserAgreementUrl();
                }
            });
        }
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        hasNewVersion = true;
        setTitleTxt(R.string.txt_setting);
        showBackImgLeft(getBackImgLeft());
        xmlUtil = new SZXmlUtil(SZConst.SETTING);
        switchRecommendAd.setChecked(xmlUtil.getBoolean("recommend_ad",true));
        switchRecommendAd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                xmlUtil.put("recommend_ad",isChecked);
            }
        });
        lltRecommendAd.setVisibility(isShowRecommendAd() ? View.VISIBLE : View.GONE);
        btnLogout.setVisibility(isShowLogout()&&isUserLogin() ? View.VISIBLE : View.GONE);
        lltAccountSafe.setVisibility(isShowAccountSafe() ? View.VISIBLE : View.GONE);
        lltCancelPrivacy.setVisibility(isShowCancelPrivacy() ? View.VISIBLE : View.GONE);
        txtCache.setText(SZFileUtil.getInstance().getAllCacheFileSize());
        if(isShowUpdate()) {
            lltUpdate.setVisibility(View.VISIBLE);
            requestNewVersionOnly();
        }else{
            lltUpdate.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void requestNewVersion() {
        showBar();
        new VersionUpdate(this, 0, new VersionUpdate.VersionUpdateListener() {
            @Override
            public void onSuccess(VersionInfo versionInfo, String data) {
                closeBar();
                if (versionInfo.isLastVersion()) {
                    hasNewVersion = true;
                    txtUpdate.setText("检查更新");
                    txtVersion.setTextColor(getResources().getColor(R.color.txtUpdateColor));
                    txtVersion.setText("有新版本");
                }else{
                    hasNewVersion = false;
                    txtUpdate.setText("已是最新版本");
                    txtVersion.setTextColor(getResources().getColor(getVersionTxtColor()<0?R.color.txtColorGray:getVersionTxtColor()));
                    txtVersion.setText("当前版本"+SZUtil.getVersionName());
                }
            }

            @Override
            public void onNetError(int errorCode, String errorMsg) {
                closeBar();
                if (errorCode == SZRetrofitManager.ERROR_EXCEPTION || errorCode == SZRetrofitManager.ERROR_NET_FAILED) {
                    SZToast.error("网络异常!");
                } else {
                    SZToast.error(errorMsg);
                }
            }

            @Override
            public void onClickCloseNoticeAlert() {

            }

            @Override
            public void onUpdateAlertDismiss(VersionInfo versionInfo) {
            }

            @Override
            public void onIgnoreUpdateAlertShow(VersionInfo versionInfo, String data) {
            }
        }).checkNewVersion(true);
    }

    private void requestNewVersionOnly() {
        request(SZRetrofitManager.getInstance().getSZRequest().getNewVersionInfoV2("ANDROID", SZUtil.getVersionCode())
                , new SZCommonResponseListener() {
                    @Override
                    public void onResponseSuccess(String data) {
                        VersionInfo versionInfo = new Gson().fromJson(data, VersionInfo.class);
                        if (versionInfo.isLastVersion()) {
                            hasNewVersion = true;
                            txtUpdate.setText("检查更新");
                            txtVersion.setTextColor(getResources().getColor(R.color.txtUpdateColor));
                            txtVersion.setText("有新版本");
                        }else{
                            hasNewVersion = false;
                            txtUpdate.setText("已是最新版本");
                            txtVersion.setTextColor(getResources().getColor(getVersionTxtColor()<0?R.color.txtColorGray:getVersionTxtColor()));
                            txtVersion.setText("当前版本"+SZUtil.getVersionName());
                        }
                    }

                    @Override
                    public void onResponseError(int errorCode, String errorMsg) {

                    }
                });
    }

    private void clearCache() {
        showWarningAlert(getString(R.string.txt_clear_cache), "确认清空所有缓存的图片和文件吗？", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                SZFileUtil.getInstance().clearAllCacheFile();
                txtCache.setText(SZFileUtil.getInstance().getAllCacheFileSize());
            }
        });
    }

    public void logout() {
        showLogoutConfirmAlert();
    }

    public void showLogoutConfirmAlert(){
        showWarningAlert("退出登录", "确认退出登录吗？", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                btnLogout.setVisibility(View.GONE);
                onLogout();
            }
        });
    }

    public void agreeCancelPrivacy(){
        showCancelPrivacyConfirmAlert();
    }

    public void showCancelPrivacyConfirmAlert() {
        final AlertWidget alertWidget = new AlertWidget(this);
        alertWidget.show(R.layout.item_sure_cancel_privacy_alert);
        TextView txtClose = alertWidget.getWindow().findViewById(R.id.txtClose);
        TextView txtAgree = alertWidget.getWindow().findViewById(R.id.txtAgree);
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
            }
        });
        txtAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                onCancelPrivacy();
            }
        });
    }

    public void cancelPrivacy(){
        showCancelPrivacyAlert();
    }

    public void showCancelPrivacyAlert() {
        final AlertWidget alertWidget = new AlertWidget(this);
        alertWidget.show(R.layout.item_cancel_privacy_alert);
        TextView txtClose = alertWidget.getWindow().findViewById(R.id.txtClose);
        TextView txtAgree = alertWidget.getWindow().findViewById(R.id.txtAgree);
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
            }
        });
        txtAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertWidget.close();
                agreeCancelPrivacy();
            }
        });
    }
    public void cancelAgreePrivacy(){
        try {
            new SZXmlUtil("privacy_alert").clear();
            new SZXmlUtil(SZConst.SETTING).clear();
            UserManager.getInstance().clearUser();
            new SZXmlUtil("isOldUser").clear();
            new SZXmlUtil("isUsedApp").clear();
            SZApp.getInstance().removeAndFinishAllActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public abstract void onClickPrivacyUrl();

    public abstract void onClickUserAgreementUrl();

    public abstract void onCancelPrivacy();

    public abstract boolean isUserLogin();

    public abstract boolean isShowAccountSafe();

    public abstract boolean isShowCancelPrivacy();

    public abstract void onLogout();

    public abstract Class getLoginClass();

    public abstract Class getAccountSafeClass();

    public abstract boolean isShowLogout();

    public abstract boolean isShowRecommendAd();

    public abstract boolean isShowUpdate();

    public int getVersionTxtColor(){
        return -1;
    }
}
