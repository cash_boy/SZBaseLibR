package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.userinfo.bean.AccountConditionInfo;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.view.sweet_alert.SweetAlertDialog;
import com.google.gson.Gson;

import io.reactivex.Observable;

/**
 * Created by CN.
 */

public abstract class SZCancellationAccountConditionActivity extends SZBaseActivity implements View.OnClickListener, SZInterfaceActivity {
    public CheckBox chkBoxAgree;
    public TextView txtAgreement;
    public Button btnNext;
    public View lltVipPower;
    public AccountConditionInfo accountConditionInfo;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_cancellation_account_condition;
    }

    @Override
    protected void onBindChildViews() {
        chkBoxAgree = findViewById(R.id.chkBoxAgree);
        txtAgreement = findViewById(R.id.txtAgreement);
        btnNext = findViewById(R.id.btnNext);
        lltVipPower = findViewById(R.id.lltVipPower);
    }

    @Override
    protected void onBindChildListeners() {
        btnNext.setOnClickListener(this);
        txtAgreement.setOnClickListener(this);
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        lltVipPower.setVisibility(isShowVipHint() ? View.VISIBLE : View.GONE);
        showBackImgLeft(getBackImgLeft());
        setTitleTxt("注销须知");
        isShowContent(false);
        onReloadData(false);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        if(getUserCancellationAccountInfoObservable()!=null) {
            request(getUserCancellationAccountInfoObservable(), new SZCommonResponseListener() {
                @Override
                public void onResponseSuccess(String data) {
                    isShowContent(true);
                    accountConditionInfo = new Gson().fromJson(data, AccountConditionInfo.class);
                }

                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    showErrorWithMsg(errorCode,errorMsg);
                }
            });
        }else{
            request(SZRetrofitManager.getInstance().getSZRequest().getCancellationAccountInfo(), new SZCommonResponseListener() {

                @Override
                public void onResponseSuccess(String data) {
                    isShowContent(true);
                    accountConditionInfo = new Gson().fromJson(data, AccountConditionInfo.class);
                }
                @Override
                public void onResponseError(int errorCode, String errorMsg) {
                    showErrorWithMsg(errorCode,errorMsg);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtAgreement) {
            onCancellationAccountClickAgreement();
        } else if (v.getId() == R.id.btnNext) {
            if (chkBoxAgree.isChecked()) {
                VipInfo vipInfo = accountConditionInfo.getLogicMember();
                if (vipInfo != null) {
                    if (vipInfo.getMemberValidityTime() > System.currentTimeMillis()) {
                        showAlert(SweetAlertDialog.WARNING_TYPE, null, "您的帐户中还有未到期的会员，如果点击同意，" +
                                "会员将作废且不可找回，是否继续注销？", "同意", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                next();
                            }
                        }, "放弃注销", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                finish();
                            }
                        }, true);
                        return;
                    }
                }
                next();
            } else {
                SZToast.warning("请先阅读并同意注销协议！");
            }
        }
    }

    private void next() {
        if (accountConditionInfo.isPhoneType()) {
            startActivity(accountConditionInfo.getLogicConsumerTripartite(),getCancellationAccountPhoneActivityClass());
        } else if(accountConditionInfo.isAlipayType()){
            startActivity(accountConditionInfo.getLogicConsumerTripartite(),getCancellationAccountAlipayActivityClass());
        }else {
            startActivity(accountConditionInfo.getLogicConsumerTripartite(),getCancellationAccountWechatActivityClass());
        }
        finish();
    }

    public abstract boolean isShowVipHint();

    public abstract void onCancellationAccountClickAgreement();

    public abstract Class getCancellationAccountPhoneActivityClass();

    public abstract Class getCancellationAccountWechatActivityClass();

    public abstract Class getCancellationAccountAlipayActivityClass();

    public abstract Observable<String> getUserCancellationAccountInfoObservable();
}
