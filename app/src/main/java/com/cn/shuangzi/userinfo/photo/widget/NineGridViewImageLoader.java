package com.cn.shuangzi.userinfo.photo.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.cn.shuangzi.GlideApp;
import com.cn.shuangzi.R;
import com.lzy.ninegrid.NineGridView;

public class NineGridViewImageLoader implements NineGridView.ImageLoader {
    @Override
    public void onDisplayImage(Context context, ImageView imageView, String url) {
        GlideApp.with(context).load(url).override(imageView.getWidth(),imageView.getHeight()).centerInside().placeholder(R.color.dividerLightGray).into(imageView);
    }


    @Override
    public Bitmap getCacheImage(String url) {
        return null;
    }
}
