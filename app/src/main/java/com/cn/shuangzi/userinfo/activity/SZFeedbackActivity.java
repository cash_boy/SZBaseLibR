package com.cn.shuangzi.userinfo.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.retrofit.SZCommonResponseListener;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.view.RangeEditText;


public abstract class SZFeedbackActivity extends SZBaseActivity implements SZInterfaceActivity {
    public RangeEditText rangeEdtTxt;
    public EditText edtContact;
    public Button btnSubmit;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void onBindChildViews() {
        rangeEdtTxt = findViewById(R.id.rangeEdtTxt);
        edtContact = findViewById(R.id.edtContact);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    @Override
    protected void onBindChildListeners() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    submit();
            }
        });
        rangeEdtTxt.setOnInputOutRangeListener(new RangeEditText.OnInputOutOfRangeListener() {
            @Override
            public void onOutOfRange() {
                SZToast.warning(getString(R.string.txt_feedback_limit));
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        onChildViewCreatedPre();
        setTitleTxt(R.string.txt_feedback);
        showBackImgLeft(getBackImgLeft());
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }
    public void submit() {
        String content = rangeEdtTxt.getText().trim();
        String contact = edtContact.getText().toString().trim();
        if (!SZValidatorUtil.isValidString(content)){
            SZToast.warning(getString(R.string.error_feedback_empty));
            return;
        }
        btnSubmit.setEnabled(false);
        showBar();
        SZRetrofitManager.getInstance().request(SZRetrofitManager.getInstance().getSZRequest().submitFeedbackV2(getUserId(), content, String.valueOf(SZUtil.getVersionCode()),contact), getRequestTag(), new SZCommonResponseListener(false) {
            @Override
            public void onResponseSuccess(String data) {
                closeBar();
                SZToast.success(getString(R.string.txt_feedback_success));
                finish();
            }

            @Override
            public void onResponseError(int errorCode, String errorMsg) {
                closeBar();
                btnSubmit.setEnabled(true);
                SZToast.error(getString(R.string.error_net_work));
            }
        });
    }
    public abstract String getUserId();
}
