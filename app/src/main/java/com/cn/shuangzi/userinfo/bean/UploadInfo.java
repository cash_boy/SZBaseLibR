package com.cn.shuangzi.userinfo.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */
public class UploadInfo implements Serializable {
    private String uploadFilePath;
    private String objPath;
    private boolean isUploaded;
    private String realFilePath;
    private String objName;
    public UploadInfo() {
    }

    public UploadInfo(String uploadFilePath, String objPath) {
        this.uploadFilePath = uploadFilePath;
        this.objPath = objPath;
    }

    public UploadInfo(String uploadFilePath, String objPath, String realFilePath) {
        this.uploadFilePath = uploadFilePath;
        this.objPath = objPath;
        this.realFilePath = realFilePath;
    }

    public UploadInfo(String uploadFilePath, String objPath, boolean isUploaded, String realFilePath) {
        this.uploadFilePath = uploadFilePath;
        this.objPath = objPath;
        this.isUploaded = isUploaded;
        this.realFilePath = realFilePath;
    }

    public String getUploadFilePath() {
        return uploadFilePath;
    }

    public void setUploadFilePath(String uploadFilePath) {
        this.uploadFilePath = uploadFilePath;
    }

    public String getObjPath() {
        return objPath;
    }

    public void setObjPath(String objPath) {
        this.objPath = objPath;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public String getRealFilePath() {
        return realFilePath;
    }

    public void setRealFilePath(String realFilePath) {
        this.realFilePath = realFilePath;
    }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }

    @Override
    public String toString() {
        return "UploadInfo{" +
                "uploadFilePath='" + uploadFilePath + '\'' +
                ", objPath='" + objPath + '\'' +
                ", isUploaded=" + isUploaded +
                ", realFilePath='" + realFilePath + '\'' +
                ", objName='" + objName + '\'' +
                '}';
    }
}
