package com.cn.shuangzi.loginplugin.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class SZUserInfo implements Serializable {

    private UserDo userConsumerDo;
    private UserTokenDo userConsumerTokenDo;

    public void setUserConsumerDo(UserDo userConsumerDo) {
        this.userConsumerDo = userConsumerDo;
    }

    public void setUserConsumerTokenDo(UserTokenDo userConsumerTokenDo) {
        this.userConsumerTokenDo = userConsumerTokenDo;
    }

    public class UserDo implements Serializable {
        public String consumerAvatarUrl;
        public String consumerId;
        public String consumerNickName;
        public long consumerCreateTime;
        public long consumerUpdateTime;
        public String consumerPhone;
        public String consumerApplicationId;

        public String getConsumerAvatarUrl() {
            return consumerAvatarUrl;
        }

        public void setConsumerAvatarUrl(String consumerAvatarUrl) {
            this.consumerAvatarUrl = consumerAvatarUrl;
        }

        public String getConsumerId() {
            return consumerId;
        }

        public void setConsumerId(String consumerId) {
            this.consumerId = consumerId;
        }

        public String getConsumerNickName() {
            return consumerNickName;
        }

        public void setConsumerNickName(String consumerNickName) {
            this.consumerNickName = consumerNickName;
        }

        public String getConsumerPhone() {
            return consumerPhone;
        }

        public void setConsumerPhone(String consumerPhone) {
            this.consumerPhone = consumerPhone;
        }

        public String getConsumerApplicationId() {
            return consumerApplicationId;
        }

        public void setConsumerApplicationId(String consumerApplicationId) {
            this.consumerApplicationId = consumerApplicationId;
        }

        public long getConsumerCreateTime() {
            return consumerCreateTime;
        }

        public void setConsumerCreateTime(long consumerCreateTime) {
            this.consumerCreateTime = consumerCreateTime;
        }

        public long getConsumerUpdateTime() {
            return consumerUpdateTime;
        }

        public void setConsumerUpdateTime(long consumerUpdateTime) {
            this.consumerUpdateTime = consumerUpdateTime;
        }

        @Override
        public String toString() {
            return "UserDo{" +
                    "consumerAvatarUrl='" + consumerAvatarUrl + '\'' +
                    ", consumerId='" + consumerId + '\'' +
                    ", consumerNickName='" + consumerNickName + '\'' +
                    ", consumerCreateTime=" + consumerCreateTime +
                    ", consumerUpdateTime=" + consumerUpdateTime +
                    ", consumerPhone='" + consumerPhone + '\'' +
                    ", consumerApplicationId='" + consumerApplicationId + '\'' +
                    '}';
        }
    }

    public class UserTokenDo implements Serializable {
        private String uctId;

        @Override
        public String toString() {
            return "UserTokenDo{" +
                    "uctId='" + uctId + '\'' +
                    '}';
        }
    }

    public String getAppId() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerApplicationId;
    }

    public void setAppId(String id) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerApplicationId = id;
    }

    public String getAvatar() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerAvatarUrl;
    }

    public void setAvatar(String avatar) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerAvatarUrl = avatar;
    }

    public String getId() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerId;
    }

    public void setId(String id) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerId = id;
    }

    public String getNickname() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerNickName;
    }

    public void setNickname(String nickname) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerNickName = nickname;
    }

    public String getToken() {
        if (userConsumerTokenDo == null) {
            return null;
        }
        return userConsumerTokenDo.uctId;
    }

    public void setToken(String token) {
        if (userConsumerTokenDo == null) {
            userConsumerTokenDo = new UserTokenDo();
        }
        userConsumerTokenDo.uctId = token;
    }

    public String getPhone() {
        if (userConsumerDo == null) {
            return null;
        }
        return userConsumerDo.consumerPhone;
    }

    public void setPhone(String phone) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerPhone = phone;
    }

    public long getUserCreateTime() {
        if (userConsumerDo == null) {
            return -1;
        }
        return userConsumerDo.consumerCreateTime;
    }

    public long getUserUpdateTime() {
        if (userConsumerDo == null) {
            return -1;
        }
        return userConsumerDo.consumerUpdateTime;
    }

    public void setUserUpdateTime(long updateTime) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerUpdateTime = updateTime;
    }

    public void setUserCreateTime(long createTime) {
        if (userConsumerDo == null) {
            userConsumerDo = new UserDo();
        }
        userConsumerDo.consumerCreateTime = createTime;
    }

    @Override
    public String toString() {
        return "SZUserInfo{" +
                "userConsumerDo=" + userConsumerDo +
                ", userConsumerTokenDo=" + userConsumerTokenDo +
                '}';
    }
}
