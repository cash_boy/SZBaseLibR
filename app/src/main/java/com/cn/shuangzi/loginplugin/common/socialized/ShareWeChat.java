package com.cn.shuangzi.loginplugin.common.socialized;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.modelbiz.WXOpenCustomerServiceChat;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXMiniProgramObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONObject;

/**
 * *******************
 * 微信分享
 *
 * @author cn
 * *******************
 */
public class ShareWeChat {
    private IWXAPI api;
    private static ShareWeChat weiXinShare;
    private String weiXinPackage = "com.tencent.mm";

    private ShareWeChat(Context context) {
        if (ThirdPlatform.WE_CHAT_APPID == null) {
            throw new IllegalArgumentException("appId not init...");
        }
        api = WXAPIFactory.createWXAPI(context, ThirdPlatform.WE_CHAT_APPID, true);
        api.registerApp(ThirdPlatform.WE_CHAT_APPID);
    }

    public static ShareWeChat getInstance() {
        if (weiXinShare == null) {
            weiXinShare = new ShareWeChat(SZApp.getInstance().getApplicationContext());
        }
        return weiXinShare;
    }

    public boolean shareCircle(String title, String desc, String url, Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = ThirdUtil.getShareString(title);
        msg.description = ThirdUtil.getShareString(desc);
        msg.thumbData = SZUtil.bmpToByteArray(bitmap, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(String title, String desc, String url, Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = ThirdUtil.getShareString(title);
        msg.description = ThirdUtil.getShareString(desc);
        msg.thumbData = SZUtil.bmpToByteArray(bitmap, true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(String desc) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.description = ThirdUtil.getShareString(desc);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareFriend(Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXImageObject imgObj = new WXImageObject(bitmap);
        WXMediaMessage msg = new WXMediaMessage(imgObj);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("imgObj");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        return true;
    }

    public boolean shareCircle(Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXImageObject imgObj = new WXImageObject(bitmap);
        WXMediaMessage msg = new WXMediaMessage(imgObj);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("imgObj");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
        return true;
    }

    public boolean shareMiniProgram(String title, String desc, String url, String originalId, String path, Bitmap bitmap) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXMiniProgramObject miniProgramObj = new WXMiniProgramObject();
        miniProgramObj.webpageUrl = url; // 兼容低版本的网页链接
        miniProgramObj.miniprogramType = WXMiniProgramObject.MINIPTOGRAM_TYPE_RELEASE;// 正式版:0，测试版:1，体验版:2
        miniProgramObj.userName = originalId;     // 小程序原始id
        miniProgramObj.path = path;            //小程序页面路径；对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"
        WXMediaMessage msg = new WXMediaMessage(miniProgramObj);
        msg.title = title;                    // 小程序消息title
        msg.description = desc;               // 小程序消息desc
        msg.thumbData = SZUtil.bmpToByteArray(bitmap, false);// 小程序消息封面图片，小于128k

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("miniProgram");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;  // 目前只支持会话
        api.sendReq(req);
        return true;
    }

    public boolean openMiniProgram(String originalId, String path) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
        req.userName = originalId; // 填小程序原始id
        req.path = path;                  //拉起小程序页面的可带参路径，不填默认拉起小程序首页
        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
        api.sendReq(req);
        return true;
    }
    public boolean openCompanyService(String corpId,String url) {
        if (!SZUtil.isAppInstalled(weiXinPackage)) {
            SZToast.warning("未安装微信客户端！");
            return false;
        }
        // 判断当前版本是否支持拉起客服会话
        if (api.getWXAppSupportAPI() >= com.tencent.mm.opensdk.constants.Build.SUPPORT_OPEN_CUSTOMER_SERVICE_CHAT) {
            WXOpenCustomerServiceChat.Req req = new WXOpenCustomerServiceChat.Req();
            req.corpId = corpId;                                  // 企业ID
            req.url = url;    // 客服URL
            api.sendReq(req);
            return true;
        }else{
            SZToast.warningLong("当前版本微信不支持！");
            return false;
        }
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    public boolean openVipService(){
        return openCompanyService(
                "ww319db112ce746335","https://work.weixin.qq.com/kfid/kfc539223cd934af350"
        );
    }
}
