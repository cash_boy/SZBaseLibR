package com.cn.shuangzi.loginplugin.common.socialized;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.net.URL;

/**
 * Created by CN.
 */

public class ThirdUtil {
    public static Bitmap getLocalBitmap(Context context, int resId) {
        Bitmap thumb = null;
        try {
            thumb = BitmapFactory.decodeResource(context.getResources(), resId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return thumb;
    }

    public static Bitmap getUrlBitmap(String imgUrl) {
        Bitmap thumb = null;
        try {
            thumb = BitmapFactory.decodeStream(new URL(imgUrl).openStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //注意下面的这句压缩，120，150是长宽。
        // 一定要压缩，不然会分享失败
//            Bitmap thumbBmp = Bitmap.createScaledBitmap(thumb, 120, 150, true);
//            thumb.recycle();
        return thumb;
    }
    public static Bitmap getBitmapThumbnail(Bitmap bitMap) {
        return getBitmapThumbnail(bitMap, 99, 99);
    }

    public static Bitmap getBitmapThumbnail(Bitmap bitMap, int widthNew, int heightNew) {
        if (bitMap == null) {
            return null;
        }
        int width = bitMap.getWidth();
        int height = bitMap.getHeight();
        // 设置想要的大小
        int newWidth = widthNew;
        int newHeight = heightNew;
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        Bitmap newBitMap = Bitmap.createBitmap(bitMap, 0, 0, width, height, matrix, true);
        return newBitMap;
    }

    public static String getShareString(String text){
        return getLimitString(text,45);
    }

    public static String getLimitString(String text,int length){
        if(text!=null){
            if(text.length()>length){
                return text.substring(0,length)+"...";
            }
        }
        return text;
    }
}
