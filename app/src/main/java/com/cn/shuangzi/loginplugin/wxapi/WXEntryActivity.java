package com.cn.shuangzi.loginplugin.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.cn.shuangzi.loginplugin.activity.SZLoginActivity;
import com.cn.shuangzi.util.SZUtil;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            SZLoginActivity.thirdLogin.getLoginWeChat().getIwxapi().handleIntent(getIntent(), this);
        } catch (Exception e) {
            finish();
        }
    }

    @Override
    public void onReq(BaseReq req) {
        SZUtil.log("req:" + req);
        finish();
    }

    @Override
    public void onResp(BaseResp resp) {
        SZUtil.log("resp:" + resp);
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                SendAuth.Resp sendResp = (SendAuth.Resp) resp;
                SZLoginActivity.thirdLogin.getLoginWeChat().setWx_code(sendResp.code);
                SZLoginActivity.thirdLogin.getLoginWeChat().onGetCodeSuccess();
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                break;
            default:
                break;
        }
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        try {
            SZLoginActivity.thirdLogin.getLoginWeChat().getIwxapi().handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }
}
