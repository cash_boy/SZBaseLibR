package com.cn.shuangzi.loginplugin.common.socialized;


import com.cn.shuangzi.SZBaseActivity;

/**
 * Created by CN on 2016/11/30.
 */

public class ThirdLogin {
    public static final String QQ_PLATFORM = "QQ_PLATFORM";
    public static final String WE_CHAT_PLATFORM = "WeChat_PLATFORM";
    public static final String ALIPAY_PLATFORM = "Alipay_PLATFORM";
    private String loginType;
    private SZBaseActivity activity;
    private OnThirdLoginResponseListener onThirdLoginResponseListener;
//    private QQLogin loginQQ;
    private AliPayLogin aliPayLogin;
    private WeChatLogin loginWeChat;
    private String authInfo;
    public ThirdLogin(SZBaseActivity activity, String loginType, OnThirdLoginResponseListener
            onThirdLoginResponseListener) {
        this.loginType = loginType;
        this.activity = activity;
        this.onThirdLoginResponseListener = onThirdLoginResponseListener;
    }
    public ThirdLogin(SZBaseActivity activity, String loginType,String authInfo, OnThirdLoginResponseListener
            onThirdLoginResponseListener) {
        this.loginType = loginType;
        this.activity = activity;
        this.authInfo = authInfo;
        this.onThirdLoginResponseListener = onThirdLoginResponseListener;
    }
    public void login(){
        if(ThirdLogin.QQ_PLATFORM.equals(loginType)){
//            loginQQ = new QQLogin(activity,onThirdLoginResponseListener);
//            loginQQ.login();
        }else if(ThirdLogin.WE_CHAT_PLATFORM.equals(loginType)){
            loginWeChat = new WeChatLogin(activity,onThirdLoginResponseListener);
            loginWeChat.login();
        }else if(ThirdLogin.ALIPAY_PLATFORM.equals(loginType)){
            loginAlipay(authInfo);
        }
    }
    public void loginAlipay(String authInfo){
        aliPayLogin = new AliPayLogin(activity,authInfo,onThirdLoginResponseListener);
        aliPayLogin.login();
    }
//    public IUiListener getQQIUiListener(){
//        if(loginQQ!=null)
//            return loginQQ.listenerQQ;
//        return null;
//    }
    public String getLoginType() {
        return loginType;
    }


    public WeChatLogin getLoginWeChat() {
        return loginWeChat;
    }

//    public QQLogin getLoginQQ() {
//        return loginQQ;
//    }

    public SZBaseActivity getActivity() {
        return activity;
    }

    public void setActivity(SZBaseActivity activity) {
        this.activity = activity;
    }

    public interface OnThirdLoginResponseListener{
        void onSuccess(String thirdId);
        void onFailed();
        void onCancel();
    }
}
